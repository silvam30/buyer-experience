<script lang="ts">
import Vue from 'vue';
import { SEARCH_SORT_OPTIONS, SEARCH_TYPE } from '../../common/constants';
import SlpSearchPagination from './search-pagination.vue';

/**
 * Component that receives the search results array and implement's the pagination component.
 * This component uses the $listeners attribute from Vue to emit the pagination event to the parent component (Search Content component) without creating boilerplate code
 */
export default Vue.extend({
  name: 'SlpSearchResults',
  components: {
    SlpSearchPagination,
  },
  props: {
    searchResults: {
      type: Object,
      required: true,
    },
    searchFilter: {
      type: String,
      required: true,
    },
  },
  data() {
    return {
      sortOptions: SEARCH_SORT_OPTIONS.map((item) => item.label),
      sortBy: SEARCH_SORT_OPTIONS[0].label,
    };
  },
  computed: {
    totalPages() {
      return this.searchResults.info.page.num_pages <= 100
        ? this.searchResults.info.page.num_pages
        : 100;
    },
    resultIcon() {
      switch (this.searchFilter) {
        case SEARCH_TYPE.HANDBOOK: {
          return 'slp-docs';
        }
        case SEARCH_TYPE.BLOG: {
          return 'slp-blog';
        }
        default: {
          return 'slp-web';
        }
      }
    },
    resultLabel() {
      if (this.searchFilter === SEARCH_TYPE.HANDBOOK) {
        return SEARCH_TYPE.HANDBOOK;
      }
      if (this.searchFilter === SEARCH_TYPE.MARKETING) {
        return 'about';
      }
      return SEARCH_TYPE.BLOG;
    },
  },
  methods: {
    onSort() {
      const sortValue =
        SEARCH_SORT_OPTIONS.find(
          (item) => (this as any).sortBy === item.label,
        ) || SEARCH_SORT_OPTIONS[0];
      this.$emit('sort', sortValue.value);
    },
    toggleFilter() {
      this.$emit('activateFilter');
    },
  },
});
</script>

<template>
  <div class="search-results">
    <div class="search-results__header">
      <SlpTypography
        tag="div"
        variant="heading5-bold"
        class="search-results__header--title"
      >
        {{
          Number(searchResults.info.page.total_result_count).toLocaleString()
        }}
        Results for "{{ searchResults.info.page.query }}"
      </SlpTypography>
      <div class="search-results__header--sort">
        <SlpTypography
          tag="label"
          for="search-sort"
          variant="body2"
          class="slp-color-text-200 slp-mr-8 no-mobile"
          >Sort By:
        </SlpTypography>
        <SlpDropdown
          id="search-sort"
          v-model="sortBy"
          class="search-results__sort"
          dropdown-label="Sort By"
          name="sort"
          :options="sortOptions"
          @input="onSort"
        />
        <SlpButton
          variant="ghost"
          name="Filter"
          class="search-results__filter-btn mobile"
          @click.native="toggleFilter"
          >Filter</SlpButton
        >
      </div>
    </div>
    <div class="search-results__content">
      <a
        v-for="(page, index) in searchResults.records.page"
        :key="`${page.title} | ${index}`"
        class="search-results__page slp-color-primary-200"
        :href="page.url"
        data-ga-location="search result"
        :data-ga-name="searchResults.info.page.query"
      >
        <div class="search-results__page--topic">
          <SlpIcon :icon="resultIcon" />
          <SlpTypography tag="h5" variant="body3" class="slp-ml-8 icon-label">
            {{ resultLabel }}
          </SlpTypography>
        </div>
        <SlpTypography
          class="search-results__page-title"
          tag="h5"
          variant="body1-bold"
        >
          {{ page.title }}
        </SlpTypography>
        <SlpTypography
          class="search-results__description"
          tag="p"
          variant="body2"
        >
          <span v-if="page.highlight.body" v-html="page.highlight.body" />
          <span v-else> {{ page.body }} </span>
        </SlpTypography>
      </a>
    </div>
    <SlpSearchPagination
      :current-page="searchResults.info.page.current_page"
      :per-page="searchResults.info.page.per_page"
      :total-pages="totalPages"
      v-on="$listeners"
    />
  </div>
</template>

<style scoped lang="scss">
.search-results {
  padding: $spacing-24;

  @media (max-width: $breakpoint-md) {
    padding: 0 $spacing-8;
    overflow-x: hidden;
  }

  &__header {
    display: flex;
    justify-content: space-between;
    padding: $spacing-16 0;
    border-bottom: 1px solid $color-surface-200;
    margin-bottom: $spacing-16;

    @media (max-width: $breakpoint-md) {
      flex-direction: column;
      border: none !important;
      padding: $spacing-8;
    }
    &--title {
      @media (max-width: $breakpoint-md) {
        margin-bottom: $spacing-24;
      }
    }
    &--sort {
      display: flex;
      align-items: center;
      justify-content: space-around;
      @media (max-width: $breakpoint-md) {
        padding-top: $spacing-24;
        border-top: 1px solid $color-surface-200;
      }
    }
  }

  &__sort {
    width: 200px;
    position: relative;
    height: 44px;

    @media (max-width: $breakpoint-md) {
      width: 258px;
    }

    &:after {
      top: $spacing-8 !important;
      right: $spacing-8 !important;
    }

    &:deep(.dropdown__select) {
      height: 44px;
      color: $color-text-200;
      padding: 12px;

      &:focus-visible {
        outline: none;
      }
    }
  }

  &__filter-btn {
    cursor: pointer;
    font-weight: $font-weight-bold !important;
    font-size: 18px;
    color: $color-text-link-100;
    padding: 0 $spacing-8;
  }

  &__content {
    display: flex;
    flex-direction: column;
  }

  &__page {
    padding: $spacing-24;
    border-radius: 4px;
    color: unset;

    &:hover {
      background-color: $color-surface-800;

      .search-results__page-title {
        color: $color-text-link-100;
      }
    }

    @media (max-width: $breakpoint-md) {
      padding: $spacing-16 $spacing-8;
      margin-bottom: $spacing-32;
    }
  }

  &__page,
  &__page-title {
    transition: all 0.4s ease-in-out;
  }

  &__page-title {
    margin-bottom: $spacing-8;

    &:deep(em) {
      color: $color-text-link-100 !important;
    }
  }

  &__page--topic {
    display: flex;
    align-items: center;
    margin-bottom: $spacing-8;

    .icon-label {
      text-transform: capitalize;
    }
  }

  &__description {
    display: -webkit-box;
    overflow: hidden;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;

    &:deep(em) {
      font-weight: $font-weight-bold !important;
      font-style: normal !important;
    }
  }
  .mobile {
    display: none;
    @media (max-width: $breakpoint-md) {
      display: block;
    }
  }
  .no-mobile {
    @media (max-width: $breakpoint-md) {
      display: none;
    }
  }
}
</style>
