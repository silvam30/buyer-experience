---
  title: Join the GitLab for Open Source Program
  description: GitLab wants to give back to the open source community by helping teams be more efficient, secure, and productive. Learn more here.
  image_title: /nuxt-images/open-graph/open-source-card.png
  image_alt: GitLab for Open Source
  twitter_image: /nuxt-images/open-graph/open-source-card.png
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab for Open Source
        subtitle: Enroll in the GitLab for Open Source Program and receive features of <a href="/pricing/" data-ga-name="link to gitlab pricing" data-ga-location="header">GitLab Ultimate</a>, self-managed or SaaS, including 50,000 compute minutes calculated at a <a href="https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html#cost-factor" data-ga-name="link to gitlab cost-factor documentation" data-ga-location="header">program-specific cost factor</a>.
        primary_btn:
          url: "#open-source-program-application"
          text: Apply Now
          data_ga_name: apply now to the open source program
          data_ga_location: header
          icon:
            name: arrow-down
            variant: product
        secondary_btn:
          url: https://about.gitlab.com/handbook/marketing/developer-relations/community-programs/opensource-program/#gitlab-for-open-source-program
          text: Learn more
          data_ga_name: learn more about the open source program
          data_ga_location: header
          variant: tertiary
          icon:
            name: chevron-lg-right
            variant: product
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "Image: gitlab for public sector"
    - name: 'overview-blocks'
      data:
        primary:
          header: Requirements
          list:
            - Every project in your <a href="https://docs.gitlab.com/ee/user/namespace/#namespaces" data-ga-name="link to namespace documentation" data-ga-location="body">namespace</a> must be published under an <a href="https://opensource.org/licenses/" data-ga-name="link to open source license" data-ga-location="body">OSI-approved open source license</a>.
            - Your organization can accept donations to sustain its work, but it can't <a href="/handbook/marketing/developer-relations/community-programs/opensource-program/#who-qualifies-for-the-gitlab-for-open-source-program" data-ga-name="link to open source requirements" data-ga-location="body">seek to make a profit</a> by selling services, by charging for enhancements or add-ons, or by other means.
            - Both your GitLab.com group or self-managed instance and your source code must be <a href="https://docs.gitlab.com/ee/user/public_access.html" data-ga-name="link to public access documentation" data-ga-location="body">publicly visible and publicly available</a>.
          description: Read <a href="https://about.gitlab.com/handbook/marketing/developer-relations/community-programs/opensource-program/#who-qualifies-for-the-gitlab-for-open-source-program" data-ga-name="link to open source requirements and exceptions" data-ga-location="body">our full list of program requirements and exceptions</a> to determine whether your project qualifies.
        secondary:
          header: Important notes
          list:
            - Program members don't receive <a href="https://about.gitlab.com/support/" data-ga-name="link to GitLab support" data-ga-location="body">support</a>.
            - You'll need to <a href="https://about.gitlab.com/handbook/marketing/developer-relations/community-programs/opensource-program/#must-members-of-the-gitlab-for-open-source-program-renew-their-memberships" data-ga-name="link to renew membership" data-ga-location="body">renew your program membership</a> annually. If you don’t renew, <a href="https://about.gitlab.com/pricing/licensing-faq/#what-happens-when-my-subscription-is-about-to-expire-or-has-expired" data-ga-name="link to licensing faq" data-ga-location="body">your account will be downgraded</a>.
            - All program members are subject to the <a href="https://about.gitlab.com/handbook/legal/opensource-agreement/" data-ga-name="link to open source agreement" data-ga-location="body">GitLab for Open Source Program Agreement</a>.
        cta:
          header: Have questions about the application?
          header_typography: heading5-bold
          link:
            variant: tertiary
            text: Read our FAQs for more information
            href: "#frequently-asked-questions"
            icon:
              name: "arrow-down"
              alt: Arrow Down Icon
    - name: 'open-source-form-section'
      data:
        title: Open source program application
        blocks:
          - title: "Before applying or renewing"
            content: |
              * **Create a GitLab account.** You can begin either a [Free-tier account or Ultimate trial](/pricing){data-ga-name="link to gitlab pricing" data-ga-location="body"}. If you're considering a [migration](/handbook/marketing/developer-relations/community-programs/opensource-program/#how-else-can-gitlab-assist-open-source-projects){data-ga-name="link to gitlab migration services" data-ga-location="body"}, note that you won’t need to have finished the migration before applying to the GitLab for Open Source Program.
              * **Take screenshots.** During the application process, you'll need to provide [three screenshots](https://docs.gitlab.com/ee/subscriptions/community_programs.html#gitlab-for-open-source){data-ga-name="link to gitlab open source application process" data-ga-location="body"} of your project. We suggest taking them in advance, since you’ll need to submit them on the second page of the application form.
          - title: "What to expect"
            content: |
              *  **Wait time.** Gitlab uses SheerID, a trusted partner, to verify that you meet the GitLab for Open Source requirements. In most cases, you can expect a decision about your application within 15 business days. You may be asked to provide additional information.
              * **After you're verified.** If you qualify for the GitLab for Open Source Program, you'll receive a verification email with specific instructions for obtaining your license. Please follow the instructions carefully.
        disclaimer:
          description: Applications will not be processed during U.S. holidays. Expect delayed responses during those periods.
          icon:
            name: error
            alt: Error Icon
        card:
          title: Help and support
          description: If you experience any issues obtaining your license in the Customer Portal please open a support ticket on the [GitLab Support Portal](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293){data-ga-name="link to gitlab support portal" data-ga-location="body"} and Select “Licensing and Renewal Problems.”
    - name: faq
      data:
        aos_animation: fade-up
        aos_duration: 500
        header: Frequently asked questions
        show_all_button: true
        groups:
        - header: ""
          questions:
            - question: Must I submit separate applications for all my open source projects?
              answer: >-
                GitLab for Open Source Program benefits apply to an entire [namespace](https://docs.gitlab.com/ee/user/namespace/?_gl=1*5nxqnl*_ga*NjczNTMyNTg1LjE2NzQwNzQ0MjU.*_ga_ENFH3X7M5Y*MTY3NTc4MzA5OC4zOC4xLjE2NzU3ODg4MzYuMC4wLjA.#namespaces){data-ga-location="body" data-ga-name="link to namespace"}. To qualify for the program, every project in the applicant’s namespace must carry an OSI-approved open source license. When you apply, submit materials for the single project you believe represents all the projects in your namespace.
            - question: Will my project receive support as part of the program?
              answer: >-
                No. Members of the GitLab for Open Source Program do not receive support as part of their program subscriptions. [Read more about seeking support](https://about.gitlab.com/handbook/marketing/developer-relations/community-programs/opensource-program/#where-can-i-find-help-with-the-gitlab-for-open-source-program){data-ga-name="link to help for open source program" data-ga-location="body"}.
            - question: Why must my projects be publicly visible?
              answer: |
                The goal of the GitLab for Open Source Program is to enable collaboration on open source projects. As a pre-condition to collaboration, people must be able to view and join an open source project. As a result, we ask that all qualifying projects are publicly visible and allow users to request access to them.
            - question: How do I choose the correct number of seats for my project?
              answer: >-
                When you activate the subcription you receive as a member of the GitLab for Open Source Program, you’ll specify the number of seats your account will require. GitLab uses a concurrent (seat) model, which refers to the maximum number of users enabled at once. Every occupied seat is counted, including the owner’s, with the exception of members with Guest permissions. We are happy to grant you the number of seats you think you’ll need for current project members, as well as any additional seats you feel your project will need as its community grows. You’ll have the opportunity to request additional seats at time of renewal.
            - question: Can I also use this license for projects that aren't open source, or to host private projects?
              answer: >-
                No. You may use the GitLab subscription license you receive from the GitLab for Open Source Program only for the publicly visible open source projects for which it was approved. In [some cases](https://about.gitlab.com/handbook/marketing/developer-relations/community-programs/opensource-program/#qualification-exceptions){data-ga-location="body" data-ga-name="link to open source qualification exceptions"}, we allow program members to host a small number of private projects if those projects contain sensitive data. Please email us at [opensource@gitlab.com](mailto:opensource@gitlab.com){data-ga-location="body" data-ga-name="link to opensource@gitlab.com email"} to ask about your case. You must obtain written permission from us in order to use the license outside of program requirements. Note that any user capable of creating new projects in a [namespace](https://docs.gitlab.com/ee/user/namespace/?_gl=1*cd6vhg*_ga*NjczNTMyNTg1LjE2NzQwNzQ0MjU.*_ga_ENFH3X7M5Y*MTY3NTc4MzA5OC4zOC4xLjE2NzU3ODk3ODUuMC4wLjA.#namespaces){data-ga-location="body" data-ga-name="link to open namespaces"} is responsible for ensuring that project complies with the GitLab for Open Source Program eligibility requirements. If you’re concerned about users creating projects that violate the program’s eligibility requirements, we recommend restricting the number of users capable of creating new projects in your namespace.
            - question: Must I renew my membership in the program?
              answer: >-
                Yes. You’ll need to renew your membership in the GitLab for Open Source Program by completing the application form annually. Our program requirements may change periodically, and we’ll need to make sure that you continue to meet them. Read and follow [the renewal procedure](https://about.gitlab.com/handbook/marketing/developer-relations/community-programs/opensource-program/#how-does-someone-renew-their-membership-in-the-gitlab-for-open-soure-program){data-ga-location="body" data-ga-name="link to open source renewal procedure"} to maintain your membership. Please begin the renewal process at least one month in advance to ensure sufficient processing time. You will receive email reminders to do so and can renew as early as three months in advance of your membership’s expiration.
            - question: What happens when my membership is about to expire or has expired?
              answer: >-
                To avoid delays, we recommend that you renew your subscription at least one month in advance of the deadline. If your license is about to expire, [here’s what to expect](https://about.gitlab.com/pricing/licensing-faq/#what-happens-when-my-subscription-is-about-to-expire-or-has-expired){data-ga-location="body" data-ga-name="link to subscription expired faq"}.
            - question: Is GitLab available in my country?
              answer: >-
                GitLab is available to people all over the world in [many languages.](https://translate.gitlab.com/?_gl=1*6p8al*_ga*NjczNTMyNTg1LjE2NzQwNzQ0MjU.*_ga_ENFH3X7M5Y*MTY3NTc4MzA5OC4zOC4xLjE2NzU3OTA0NjQuMC4wLjA){data-ga-location="body" data-ga-name="link to gitlab translations"}. However, since we are a U.S.-based company, we do not offer our services in U.S. embargoed countries.
            - question: Why can't I see an application form on this page?
              answer: >-
                Some web browsers and privacy extensions block the application form because they identify it as a pop-up. Please disable the pop-up blocker on this page, refresh the page, and check again.

