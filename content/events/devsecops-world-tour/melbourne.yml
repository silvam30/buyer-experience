---
  title: "Melbourne World Tour"
  og_title: Melbourne World Tour
  description: Join us and commit to a day of DevOps inspiration and innovation at GitLab Commit. Now accepting talk submissions!
  og_description: Join us and commit to a day of DevOps inspiration and innovation at GitLab Commit. Now accepting talk submissions!
  twitter_description: Join us and commit to a day of DevOps inspiration and innovation at GitLab Commit. Now accepting talk submissions!
  og_image: /nuxt-images/events/world-tour/cities/melbourne_illustration.png
  twitter_image: /nuxt-images/events/world-tour/cities/melbourne_illustration.png
  time_zone: Australian Eastern Standard Time (AEST)
  registrationClosed: true
  hero:
    breadcrumbs:
      - title: DevSecOps World Tour
        href: /events/devsecops-world-tour/
      - title: Melbourne
    header: Melbourne
    subtitle: August 17, 2023
    location: |
      Novotel Melbourne on Collins
      270 Collins Street
      Melbourne VIC 3000, Australia

    image:
      src: /nuxt-images/events/world-tour/cities/melbourne_illustration.png
    button:
      text: Register for Melbourne
      href: '#registration-form'
      variant: secondary
      icon:
        name: arrow-down
        variant: product
        size: sm
  agenda:
    - time: 9:00 am
      name: Registration & Breakfast
      speakers:
    - time: 10:00 am
      name: |
        Welcome Keynote: GitLab DevSecOps World Tour - The Evolution of DevSecOps
      speakers:
        - name: Ashley Kramer
          title: Chief Marketing and Strategy Officer
          company: GitLab
          image:
            src:  /nuxt-images/events/world-tour/speakers/ashley-kramer.jpg
          biography:
            text: |
              Ashley Kramer is GitLab's Chief Marketing and Strategy Officer. Ashley leverages her leadership experience in marketing, product, and technology to position GitLab as the leading DevSecOps platform. She also leads the strategy for product-led growth and code contribution to the GitLab platform.

              Prior to GitLab, Ashley was CPO and CMO of Sisense and has held several leadership roles, including SVP of Product at Alteryx and Head of Cloud at Tableau, as well as marketing, product, and engineering leadership roles at Amazon, Oracle, and NASA.
      description:
        title: 'Welcome Keynote: GitLab DevSecOps World Tour - The Evolution of DevSecOps'
        text: |
          Join us at the GitLab DevSecOps World Tour in Melbourne where we'll be showcasing a comprehensive overview of the evolving DevSecOps landscape. Learn how Artificial Intelligence (AI) is set to impact all teams across the SDLC and how we envision organizations confidently securing their software supply chain while staying laser focused on delivering business value.
    - time: 10:30 am
      name: Next up for The DevSecOps Platform
      speakers:
        - name: Mike Flouton
          title: VP, Product Management
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Mike Flouton.jpg
          biography:
            text: Mike Flouton, VP of Product Management at GitLab, leads the CI/CD, technical enablement, and SaaS product teams. With nearly 25 years in cybersecurity, cloud, SaaS and enterprise software, Mike previously served as VP of Products at Barracuda Networks and VP of Product Marketing at BAE Systems. He is also an angel investor and startup advisor. A lifelong technology enthusiast, Mike began his career as a software engineer and holds a BS from Cornell University.
        - name: Justin Farris
          title: Senior Director, Product
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Justin Farris.jpeg
          biography:
            text: Justin Farris is currently the Senior Director of Product at GitLab. With a background in Product Management, Strategy and Growth across numerous verticles. Justin brings a wealth of knowledge to help teams grow, scale & succeed. Prior to his role at GitLab, Justin served as the Director of Product at Zillow overseeing Growth teams.
        
      description:
          text: |
            Teams are facing more pressure than ever to deliver software faster — and software development processes are undergoing transformative changes. With recent AI innovations, we see teams developing, securing and deploying software very differently. Join us to hear how our vision for GitLab is set to change how teams function across the entire SDLC, followed by a deep dive into our latest innovations where you will learn how to keep security at the forefront powered by intelligent automations.
    - time: 11:15 am
      name: Break
    - time: 11:30 am
      name: In conversation with Ciarán Hennessy and Vanessa Love
      description:
        text: Join us in this fireside chat with Ciaran and Vanessa, to learn how they successfully transformed DevOps processes for their teams. You will gain insights on how they approached roadblocks and challenges which in turn helped them to emerge victorious in their DevSecOps journey. You will also learn how they effectively implemented DevSecOps best practices into their process to help their teams work smarter and deliver software faster. In this unique opportunity we also dig deeper into how leading organizations respond to rapidly evolving business needs and economic conditions, with minimal impact to their delivery velocity.
      speakers:
        - name: Ciarán Hennessy
          title: Chief Architect - Lendlease Digital
          company: Lendlease
          biography:
            text: Ciaran is the Chief Software Architect for Lendlease Digital, he has over 23 years of experience across multiple technology roles from Software Engineer to Enterprise Architect. He has been involved in the selection, creation, and implementation of hundreds of solutions for Lendlease. This diverse history led to Ciaran being offered the opportunity to join Lendlease Digital as one of the first 10 employees bringing his years of experience to the creation of a new software platform called Podium. This new business unit focused on the creation of the Podium platform and products to push Lendlease and the industry into the new ways to enhance and improve the property and construction process, with sustainability, safety and innovation at its core.
          image:
            src: /nuxt-images/events/world-tour/speakers/Ciaran Henessy.jpeg
        - name: Vanessa Love
          title: DevSecOps Community Advocate
          company: '  '
          biography:
            text: With a career in technology spanning 20 years, Vanessa Love has worn many hats in the DevSecOps world. Her experience has been working in small business, to local tech startup, a stint in government, through to a large enterprise company. Her passion is to bring DevSecOps practices deep into the operational side of business and prove that it makes jobs easier, more efficient and enjoyable. Recently she has enjoyed building a team in an outsourced environment and helping drive key DevSecOps practices to ensure speed and quality of delivery. 
          image:
            src: /nuxt-images/events/world-tour/speakers/Vanessa Love.jpeg
        - name: Craig Nielsen
          title: VP, Sales APAC - GitLab
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Craig Nielsen.jpeg
          biography:
            text: '   '          
    - time: 12:15 pm
      name: Lunch & Networking
    - time: 1:30 pm
      name: |
        **Workshop:** How to optimize your DevSecOps workflow by focusing on Value Streams
      speakers:
        - name: David Astor
          title: Solutions Architect Manager
          company: GitLab
          biography:
            text: |
              "David Astor is a Solutions Architect Manager with GitLab.  He's worked with numerous organizations of all sizes to assist them in simplifying their DevOps workflows. He has a great passion for learning new things and loves getting to the “wow!” moment with the folks he helps.

              He's a listener, a story teller, an agilist, a part time runner, an avid college football fan and loyal husband and friend. He's a father to an incredible daughter and general purveyor of all manner of useless trivia.

              He's a huge proponent of breaking down walls and getting people to work together. Oh, and dad jokes."
          image:
            src: /nuxt-images/events/world-tour/speakers/David Astor.png
        
        - name: Adrian Smolski
          title: Senior Manager, Solutions Architecture (APJ)
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Adrian Smolski.png
          biography:
            text: '   '
          
        - name: Tomasz Skora
          title: Senior Solutions Architect
          company: GitLab
          biography:
            text: Tomasz Skora is a Senior Solutions Architect at GitLab, based in Melbourne. He helps organisations to achieve strategic objectives, focusing on DevSecOps excellence and digital transformation. He has demonstrated a significant experience in implementing and driving impactful DevSecOps initiatives, resulting in improved software delivery, compliance and security.
          image:
            src: /nuxt-images/events/world-tour/speakers/Tomasz Skora.jpg

        - name: Ben Ridley
          title: Solutions Architect
          company: GitLab
          biography:
            text: Ben is a DevOps fanatic who loves technology and using it to make life easier. Ben started his career as an accountant, but a love of computer science turned him into a DevOps engineer before too long. Now he works as a Solutions Architect, bringing his knowledge of DevOps and GitLab that he gained over his years as an engineer to help solve his customers problems.
          image:
            src: /nuxt-images/events/world-tour/speakers/Ben Ridley.jpeg

        - name: Rob Williams
          title: Customer Success - Solutions Architect
          company: GitLab
          biography:
            text: |
              Rob's worked as a Consultant and a Solutions Architect in Software Engineering, Cyber Security, and DevSecOps. He's worked across many industry verticals allowing him to see best practices from across the board, this includes financial services, healthcare and the public sector across APAC. 

              His main areas of expertise focus on digital transformations including software architecture, toolchain consolidation, agile processes, Kubernetes administration, and full-stack development. He's worked with all three major cloud vendors to deliver DevSecOps processes and as part of the software development lifecycle.
          image:
            src: /nuxt-images/events/world-tour/speakers/Robert Williams.jpg

        
        
      description:
        text: |
          If your team is looking to optimize your DevOps practices, join the DevOps Value Stream experts for valuable insights and actionable strategies. This interactive workshop will focus on discovery, identification and removal of blockers to drive visibility and continuous improvement. Dive deeper into Value Stream Assessments (VSA) and uncover areas of improvement in the SDLC. You will learn how you can accelerate value streams for faster business value realization. This interactive exercise will also provide practical experience for you to apply these VSA methodologies to your teams to identify key areas for improvement. At the end of this session, you will have a solid understanding of VSA principles, practical tools and techniques to set you ahead on the path to improving your DevOps processes.
    - time: 3:00 pm
      name: Break
    - time: 3:15 pm
      name: |
        Partner Spotlight: DevOps1 
      speakers:
        - name: Rad Kanapathipillai
          title: Head of Engineering
          company: DevOps1
          image:
            src: /nuxt-images/events/world-tour/speakers/Rad Kanapathipillai.jpg
            contain: true
          biography:
            text: |
              Rad, Head of Engineering at DevOps1, is a seasoned tech expert with 15+ years in the industry. 
              Specialising in Platforms and Security, Rad has led platform and DevSecOps transformations at organisations like Spark New Zealand, ANZ Government, and top financial institutions. 
              Rad has also contributed significantly at Ampion/Wipro's cybersecurity division, Shelde where he was a Cloud Consulting partner. 

      description: 
        text: |
          Join us and hear from DevOps1 on how they are uniquely positioned to power cloud innovation and deliver customer value alongside business transformation. Learn how, with the combined strength of GitLab and DevOps1, customers are achieving their business goals with an accelerated pace of innovation and faster time to market.

              
    - time: 3:40 pm
      name: |
        **Roundtable discussion:** The human element of DevSecOps
      description:
          text: |
            'Technology isn't the hard problem anymore, culture seems to be the chief problem now.'- Is this a statement that you have come across?

            We invite you to discuss with leaders the role and impact of people on DevOps processes. Dig deeper into why communication, collaboration and empathy play a critical role in effective DevOps strategies. What are the strategies that work while introducing DevOps into businesses. Does it work if you find the core problem the organization faces and start there? Is education on agile a great place to start when thinking about building a DevOps culture? Join us and weigh in on the best practices to foster teamwork, promote accountability and  develop a human-centered design for DevOps processes.

    - time: 4:10 pm
      name: Closing Remarks
      speakers:
        - name: Adrian Smolski
          title: Senior Manager, Solutions Architecture
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Adrian Smolski.png          
    - time: 4:30 pm
      name: Snacks + Networking
  sponsors:
    - img: /nuxt-images/logos/devops1_logo2.png
      alt: DevOps 1
  form:
    multi_step: true
    header: Register for DevSecOps World Tour in Melbourne
    confirmation:
      - Thank you for registering!
      - Please check your email for a confirmation email.
    form_id: '3746'
  conduct_banner:
    header: Code of conduct
    text: GitLab is committed to providing a safe and welcoming experience for every attendee at all of our events whether they are virtual or onsite. Please review our [code of conduct](/company/culture/ecoc/) to ensure DevSecOps World Tour is a friendly, inclusive, and comfortable environment for all participants.
