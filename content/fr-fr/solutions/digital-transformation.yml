---
  title: Comment accélérer la transformation numérique
  description: Découvrez la transformation numérique avec GitLab.
  components:
    - name: 'solutions-hero'
      data:
        title: Accélération de la transformation numérique
        subtitle:  Une livraison plus rapide des logiciels accélère la transformation numérique. Apprenez à diriger la transformation de votre équipe avec un leader du secteur désigné par Forrester.
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          text: Regarder le webinaire
          url: /webcast/justcommit-reduce-cycle-time/
          data_ga_name: software delivery webinar
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true          
          alt: "Image: accélération de la transformation numérique avec GitLab"
    - name: 'copy-media'
      data:
        block:
          - header: Pourquoi la transformation numérique?
            id: why-digital-transformation
            miscellaneous: |

              La transformation numérique consiste à créer de nouvelles opportunités pour votre entreprise afin de stimuler l'innovation et l'efficacité, d'améliorer le fonctionnement de vos équipes et de devancer vos concurrents, le tout dans le but de proposer des expériences client innovantes et optimisées.

              Tous les secteurs sont en pleine transformation. Les clients ont des attentes plus élevées et il est de plus en plus difficile de les fidéliser, car vos concurrents sont désormais à portée de clic. Quel que soit votre secteur d'activité, la technologie doit maintenant être un aspect central de votre offre, car la concurrence provient de sources inattendues. Vous aimeriez connaître des exemples concrets ? La plus grande entreprise de taxis au monde ne possède aucun véhicule. Le plus grand fournisseur d'hébergement au monde ne possède aucune propriété. Quant au plus grand réseau de contenu au monde, il ne possède aucun contenu.

              La transformation numérique n'est plus seulement souhaitable, elle est nécessaire.
            metadata:
              id_tag: why-digital-transformation
          - header: La clé du succès
            id: unlocking-success
            miscellaneous: |
                La transformation numérique remodèle les secteurs d'activité en perturbant les entreprises et les modèles d'exploitation existants. Il ne s'agit pas de décider s'il faut adopter la transformation numérique. La question est de savoir à quelle vitesse nous pouvons y arriver pour rester compétitifs. Il est important de moderniser l'informatique pour permettre la transformation numérique, mais il est essentiel de créer une culture numérique dans toute l'organisation. Si la culture n'est pas adaptée, le succès de telles initiatives risque d'être compromis : [selonMcKinsey](https://www.mckinsey.com/industries/retail/our-insights/the-how-of-transformation), 70 % des « programmes de changement » n'atteignent pas leurs objectifs énoncés.

                Comment les organisations peuvent-elles alors réussir ? Sur la base de [cette étude de McKinsey](https://www.mckinsey.com/business-functions/people-and-organizational-performance/our-insights/unlocking-success-in-digital-transformations), les indicateurs de réussites se répartissent en cinq catégories clés

                * Avoir en place les bons leaders, ayant de l'expérience dans le numérique
                * Renforcer les capacités de la main-d'œuvre future
                * Donner aux employés les moyens d'innover dans leurs méthodes de travail
                * Effectuer une mise à niveau numérique des outils quotidiens
                * Communiquer fréquemment via des méthodes traditionnelles et numériques

                GitLab facilite la réalisation de quatre des cinq indicateurs clés identifiés pour réussir la transformation numérique. En effet, nous [suivons ces principes](/company/all-remote/){data-ga-name = "all remote" data-ga-location = "body"} au sein de GitLab pour embaucher des employés travaillant entièrement à distance et créer un environnement de travail collaboratif.
            metadata:
              id_tag: unlocking-success
          - header: Le chemin vers la transformation
            id: path-to-transformation
            miscellaneous: |

                La première phase de la transformation numérique consiste à identifier les objectifs de l'initiative numérique. Alors que 96 % des entreprises se lancent dans des initiatives numériques [selon Gartner](https://www.gartner.com/smarterwithgartner/cio-agenda-2019-digital-maturity-reaches-a-tipping-point/), la transformation numérique est désormais une priorité pour la plupart des DSI. À partir de conversations menées avec des milliers de petites, moyennes et grandes organisations, nous avons constaté que les sociétés initient le processus de transformation numérique pour atteindre trois objectifs principaux :

                **Accroître l'efficacité opérationnelle**

                Les améliorations apportées à l'efficacité sont les facteurs fondamentaux qui incitent les organisations à mettre en place la transformation numérique dans le but de remplacer les tâches manuelles et répétitives par des solutions automatisées et intégrées qui améliorent l'efficacité opérationnelle et la satisfaction des développeurs.

                **Livrer de meilleurs produits plus rapidement**

                Pour se positionner et faire face à leurs concurrents, les entreprises tirent parti de la transformation numérique pour créer des expériences client améliorées dans le but d'acquérir de nouveaux clients et de les fidéliser. Pour y parvenir, les organisations cherchent à passer de processus cloisonnés sans collaboration à un processus intégré permettant aux équipes de mieux communiquer et d'éviter l'attente et les transferts. Le but est de livrer les produits plus rapidement aux clients.

                **Réduire les risques de sécurité et de conformité**

                Selon une enquête sur les organisations de grande taille menée par IDC US DevOps en 2019, la sécurité/gouvernance a été identifiée comme l'un des trois points faibles principaux des organisations et constitue le principal domaine d'investissement au cours des deux à trois prochaines années. La nécessité de réduire le risque sécuritaire, de diminuer les perturbations dues à la sécurité et d'optimiser les audits incite les organisations à investir dans la transformation numérique.

                Une fois que les facteurs ont été identifiés, il est essentiel de vous assurer que vous ne perdez pas la réussite de vue. Chacun de ces objectifs nécessite des résultats commerciaux concrets et mesurables pour présenter un scénario avant et après.
            metadata:
              id_tag: path-to-transformation
          - header: Mesurer le succès
            id: measuring-success
            metadata:
              id_tag: measuring-success
            miscellaneous: |

              La plupart des entreprises ont du mal à utiliser les indicateurs associés à la transformation numérique, soit parce qu'ils sont confus, soit parce qu'il s'agit d'indicateurs génériques qui ne sont pas pertinents pour l'entreprise. Si les métriques adéquates ne sont pas identifiées, les résultats d'une transformation numérique sont extrêmement difficiles à mesurer.

              Les entretiens menés avec les organisations par rapport à leurs objectifs ont fait ressortir plusieurs indicateurs commerciaux clés qui peuvent être pertinents pour mesurer le succès en fonction de l'objectif. Bien sûr, cette liste n'est pas exhaustive et doit être adaptée en fonction des besoins de l'organisation.

              **Accroître l'efficacité opérationnelle**

                * Coût total de propriété (licence, ETP, coût de calcul) de la chaîne d'outils
                * Durée de cycle
                * Fréquence de déploiement
                * Turnover des développeurs
                * Satisfaction des employés


              **Livrer de meilleurs produits plus rapidement**

                * Chiffre d'affaires
                * Nombre de clients
                * Part de marché
                * Pourcentage de releases publiées à temps sans dépasser le budget
                * Durée de cycle ; durée par étape et temps d'attente ; productivité par développeur
                * Temps moyen de résolution (MTTR)


              **Réduire les risques de sécurité et de conformité**

                * Pourcentage de code analysé
                * Pourcentage de vulnérabilités de sécurité critiques qui n'ont pas été identifiées lors du développement
                * Taux de réussite des audits ; temps consacré aux audits
                * Temps moyen de résolution (MTTR) des vulnérabilités de sécurité

          - header: GitLab vous assiste
            id: git-lab-can-help
            metadata:
              id_tag: gitlab-can-help
            miscellaneous: |

              GitLab peut vous aider à atteindre les objectifs de transformation numérique mentionnés grâce à la plateforme DevSecOps la plus complète. Nous pouvons vous aider à simplifier votre chaîne d'outils de livraison de logiciels, en abandonnant les plugins, en simplifiant l'intégration et en aidant vos équipes à revenir à ce qu'elles font de mieux : développer d'excellents logiciels.

              GitLab vous accompagne tout au long du cycle de livraison de vos logiciels, du concept à la production. Plutôt que d'organiser le travail et les outils suivant une séquence d'étapes et de transferts, ce qui crée des silos et des problèmes d'intégration, GitLab favorise la collaboration dans l'ensemble de l'organisation, en donnant de la visibilité sur le flux de travail, le processus, la sécurité et la conformité tout au long du cycle de vie DevSecOps

              En savoir plus sur la façon dont GitLab peut prendre en charge votre parcours de transformation numérique

              [Gestion de la chaîne de valeur](https://about.gitlab.com/solutions/value-stream-management) et [métriques DORA](https://about.gitlab.com/solutions/value-stream-management/dora/) pour stimuler la productivité du département d'ingénierie

              En savoir plus sur les [parcours client GitLab](/customers/){data-ga-name ="customers" data-ga-location = "body"}

              Voir [ce que les analystes pensent de GitLab](/analysts/){data-ga-name ="analysts" data-ga-location = "body"}

              Vous recherchez plus de ressources sur GitLab ? [Accédez à la documentation](https://docs.gitlab.com/){data-ga-name="documentation" data-ga-location="body"} ou [consultez le blog](/blog/){data-ga-name="blog" data-ga-location="body"}
    - name: copy-resources
      data:
        block:
            - metadata:
                id_tag: resources
              title: Ressources
              text: |

                Voici une liste de ressources à propos de la transformation numérique que nous trouvons particulièrement utiles. Si vous avez des recommandations de livres, de blogs, de vidéos, de podcasts et d'autres ressources qui offrent des informations précieuses sur la définition ou la mise en œuvre de la pratique, nous aimerions les connaître.
                Partagez vos favoris avec nous sur Twitter [@GitLab](https://twitter.com/gitlab)!
              resources:
                report:
                  header: Rapports
                  links:
                    - text: Réussir les transformations numériques, par McKinsey
                      link: https://www.mckinsey.com/business-functions/people-and-organizational-performance/our-insights/unlocking-success-in-digital-transformations
                    - text: Initiative de transformation numérique du Forum économique mondial, par le Forum économique mondial
                      link: http://reports.weforum.org/digital-transformation/wp-content/blogs.dir/94/mp/files/pages/files/dti-executive-summary-20180510.pdf
                    - text: Améliorer les chances de succès, par McKinsey
                      link: https://www.mckinsey.com/business-functions/mckinsey-digital/our-insights/digital-transformation-improving-the-odds-of-success
                    - text: Comment mesurer les progrès de la transformation numérique, par Gartner
                      link: https://www.gartner.com/smarterwithgartner/how-to-measure-digital-transformation-progress
