---
  title:  Nebulaworks
  description: How Nebulaworks replaced 3 tools with GitLab and empowered customer speed and agility
  image_title: /nuxt-images/blogimages/nebulaworks.jpg
  image_alt: 
  twitter_image: /nuxt-images/blogimages/nebulaworks.jpg
  data:
    customer: Nebulaworks
    customer_logo: /nuxt-images/logos/nebulaworks-logo-blue.png
    heading: How Nebulaworks replaced 3 tools with GitLab and empowered customer speed and agility
    key_benefits:
      - label: End-to-end visibility
        icon: visibility
      - label: Simplified workflow
        icon: monitor
      - label: Enhanced collaboration
        icon: collaboration-alt-4
    header_image: /nuxt-images/blogimages/nebulaworks.jpg
    customer_industry: Technology
    customer_employee_count: 15
    customer_location: Irvine, CA
    customer_solution: |
      [GitLab Enterprise](/pricing/){data-ga-name="enterprise solution" data-ga-location="customers hero"}
    blurb: Nebulaworks adopted GitLab, eased tool maintenance and overhead, and achieved CI organization.
    introduction: |
      The engineering consultancy organization adopted GitLab for source code management (SCM), continuous integration and delivery (CI/CD), and issue tracking, and increased connection with the marketing team in the process.
    quotes:
      - text: |
          When we adopted GitLab, we went all in, because it simplified a lot of the day-to-day maintenance. We don't have a lot of time to deal with the platform where we're storing our code. It frees us up to do things that are either internal to the engineering team or to focus on customer interactions.
        author: Rob Hernandez
        author_role: CHIEF TECHNOLOGY OFFICER
        author_company: Nebulaworks
    content:
      - title: 'Consultancy by engineers for engineers'
        description: |
          Nebulaworks is a software engineering consultancy firm that provides customers with innovative solutions for development and delivery processes. The organization prides itself around working with its customers to create high-performing engineering teams where members are inspired to collaborate openly, incentivized to gather new knowledge and skills, and are fulfilled by solving complex problems simply.

          [Nebulaworks](https://www.nebulaworks.com/about/) was founded in 2014 by two engineers who saw a need to challenge the status quo of software development and IT Ops services delivery in large enterprises. Different from many of the consulting firms and Global SI’s of the time, Nebulaworks was built to solve the complex challenges of the enterprise IT engineer. A consulting and SI firm, built for engineers by engineers.
      - title: Three tools too many
        description: |
          Nebulaworks was looking for a platform that provided remote repositories to empower teams to collaborate — regardless of location. The development team was previously using a self managed instance of a git repository and a separate issue board software for issues and tracking. They wanted to increase productivity and focus their engineering efforts on development that would impact the business, rather than dealing with the daily administrative tasks just to keep the system online.

          The organization had self-managed continuous integration service that was backed by Kubernetes. This was not an ideal solution due to the administrative overhead and caused more work for the engineers who were using the system.

          Nebulaworks was maintaining a total of three internal tools for the course of several years. It was a full-time job for an engineer to manage and maintain the tools, which reduced time for software engineering. On top of that, having data and user permissions in various places caused a lot of context switching, which was time-consuming and inefficient.
      - title: One platform, many functionalities
        description: |
          Before renewing the license for the existing three internal, self-managed tools, Rob Hernandez, Chief Technology Officer, and his team researched other platforms. When they demoed GitLab, they mirrored an existing project, fit it for the CI portion to test, and then wrapped all the issue tracking and board structures. Hernandez found that GitLab’s level of organization and the ability to provide a hierarchy of different projects stood out against competitors.

          “Realizing that we could even have all the issues rolled up to the top-level GitLab group was really cool. We wouldn’t be able to do that with our existing self-managed git service,” Hernandez said. “Going through the tool in the demo, that was great. And realizing that with the hierarchy we could have subgroups, and we could break those subgroups into how we organize projects for a given customer.”

          GitLab offered the team a singular platform for CI integration, code management, collaboration, and issue tracking without the need to layer any tools. Nebulaworks is able to provide customers with a collaborative and transparent experience. A focus on a transparent relationship reduces cost for everyone by enabling faster resolution of issues, and reduces risk by creating trust and enabling both sides to plan and execute accordingly. With GitLab, Nebulaworks was able to truly focus on deliverables instead of performing updates and toolchain maintenance.
      - title: CI, code management, and customer success
        description: |
          GitLab breaks down silos as a centralized platform for collaboration, helping to drive the company forward. The team now has a simplified workflow, including issues that are close to the code, end-to-end visibility, easily integrated CI, and no more context switching between tools.
          
          Nebulaworks fully replaced its internal, self-managed git stack with GitLab. “We went as far as to define all of our resources in GitLab (repositories, groups, permissions, etc.) using Terraform. That way, GitLab gets changed just like any other piece of code – submit a MR, apply it and merge it,” Hernandez said. “It’s really cool to see new hires add their permissions on the first day via a MR and that’s the way it should be. There’s no other way for someone to make a change within our GitLab Nebulaworks group.”

          Nebulaworks selected GitLab Gold, because the SaaS capabilities allowed the team to shut down some on-prem machines and gain the benefits of a hosted offering. GitLab is powering their [deployments across Amazon Web Services (AWS)](/blog/2020/03/24/from-monolith-to-microservices-how-to-leverage-aws-with-gitlab/){data-ga-name="deployments across AWS" data-ga-location="customers content"}, specifically their container workloads running on top of Amazon EKS clusters.

          By moving to SaaS, the team is able to optimize its efficiencies by leveraging the GitLab infrastructure, and to focus on delivering better products to customers. “We’re not worrying about security patches or upgrading to new versions for new features. All of those things are taken care of by GitLab,” Hernandez said. “Now we are focusing on enabling our engineering team as a whole, across all the services and functionality that we need. Gitlab allows us to focus on that instead of focusing on maintenance.”

          Because the engineering team works with many different tools with different clients, they need to focus on the statement of work. The team measures success against what gets delivered and the time it takes to deliver, which requires a dependable tool that can work with a variety of other tools. “With GitLab, we release every two weeks to production. That’s a business need. That’s how we want to do it. It’s easy for us. It’s low stress. We correctly test things, let them bake in the development and staging capacity, before they get out to production,” Hernandez said.

          The engineering team at Nebulaworks is not the only one using GitLab. To help improve coordination between marketing and the engineering group, the content marketing team [collaborates in GitLab](/topics/version-control/software-team-collaboration/){data-ga-name="collaborates in gitlab" data-ga-location="customers content"}. The company had planned on creating content for the engineering consultancy and GitLab provided a simple way to work closely with the engineering team to create quality content. Both teams use GitLab issues and boards to communicate and content is added to the website in merge requests.

          “When we decided to invest in content marketing, we knew we had to figure out a solution that would allow marketing and engineering to work seamlessly together. The simplicity of GitLab’s features made that possible for us,” said Anne Lin, Marketing and Brand Manager. “The marketing team quickly adopted the engineering team’s workflow using issue tracking, kanboards, and merge requests to collaborate on content production. By leveraging the same workflow, the two teams were able to generate trust and visibility into each other’s work.”

          Using GitLab means that the teams can work asynchronously. Working from home is optional at Nebulaworks. As the company has adopted the work from home lifestyle, they’ve been able to collaborate easily. “We have not missed a beat. How we collaborate with our customers, how we work with our customers, how we work on projects, that workflow has not changed,” said Patrick Collins, VP, Sales and Customer Success. “It’s been a huge success having this process in place, going to a large group coming into the office, now 100% remote.”
    customer_success_stories:
      title: Customer success stories
      link:
        text: See all stories
        href: /customers/
        ga_name: all customers
        ga_location: customers stories
      read_story: Read story
      stories:
        - title: Goldman Sachs
          image: /nuxt-images/customers/goldman_sachs_case_study.png
          description: Goldman Sachs improves from 1 build every two weeks to over a thousand per day
          link: /customers/goldman-sachs/
          ga_location: customers stories
        - title: Siemens
          image: /nuxt-images/customers/siemens.png
          description: How Siemens created an open source DevOps culture with GitLab
          link: /customers/siemens/
          ga_location: customers stories
        - title: Fanatics
          image: /nuxt-images/customers/fanatics_case_study.png
          description: Fanatics' successful GitLab CI transition empowers innovation cycles and speed
          link: /customers/fanatics/
          ga_location: customers stories
