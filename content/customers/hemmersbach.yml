---
  title: Hemmersbach
  description: Eliminated 1 week of cycle time and increased ideas into production by 31 percent
  image_title: /nuxt-images/blogimages/hemmersbach_case_study.jpg
  image_alt: 
  twitter_image: /nuxt-images/blogimages/hemmersbach_case_study.jpg
  data:
    customer: Hemmersbach
    customer_logo: /nuxt-images/customers/hemmersbach_logo.svg
    customer_logo_full: true
    heading: Hemmersbach reorganized their build chain and increased build speed 59x
    key_benefits:
      - label: Faster cycle time
        icon: accelerate
      - label: Improved collaboration
        icon: bulb-bolt
      - label: Impoved code quality
        icon: increase
    header_image: /nuxt-images/blogimages/hemmersbach_case_study.jpg
    customer_industry: Technology
    customer_employee_count: 3,400
    customer_location: Germany, Poland, Hungary, Bulgaria, Austria
    customer_solution: |
      [GitLab Ultimate](/pricing/ultimate/){data-ga-name="ultimate solution" data-ga-location="customers hero"}
    sticky_benefits:
      - label: more builds per day
        stat: 60x
      - label: automated tests daily
        stat: '530'
      - label: automated daily deploys
        stat: '30'
    blurb: Global IT service provider Hemmersbach was struggling with multiple tools, difficult build cycles, and an out of sync planning process. With Gitlab Ultimate, Hemmersbach is now able to connect the portfolio planning process with actual work to deliver new capabilities.
    introduction: |
      With over 4,000 permanent colleagues in more than 40 subsidiaries worldwide, Hemmersbach enables Device as a Service for the IT industry in 190+ countries. DaaS combines hardware, software, lifecycle services, and financing into a single contract with a fee per device.
    quotes:
      - text: |
          We renewed the complete process. We didn't just take what was in Jenkins and move to GitLab, we redid everything. We took all of the findings we had from Jenkins, which wasn’t good, and we took what was good and we set it up completely new. We reinvented the wheel, just to make it better.
        author: Alexander Schmid
        author_role: Head of Software Development
        author_company: Hemmersbach
      - text: |
          GitLab is the one tool that connects our whole team. You always see GitLab open and everything is based on GitLab. GitLab is the backbone of our software development.
        author: Alexander Schmid
        author_role: Head of Software Development
        author_company: Hemmersbach
    content:
      - title: Providing IT support on a global scale
        description: |
          Hemmersbach provides vital support for the leading companies in the IT industry. The company offers global services for manufacturers and outsourcing companies to meet the wide variety of needs of their end customers. They provide technology services for thousands of organizations and specialize in supporting hardware of all sizes, from printers to the largest servers.
      - title: Collaboration and speed at a global scale
        description: |
          Hemmersbach has three teams working on the company’s proprietary software. A few years ago, they faced challenges around these teams working disparately. They struggled with collaboration and geography — with speed suffering as a result. They were using a scrum framework with a toolchain that included Jira, Jenkins, Tulip, and a variety of open source tools. With this complex toolchain, teams lacked traceability and had to overcome inefficiencies.

          “Most of our code was not connected to our process,” explained Alexander Schmid, Head of Software Development. “We had tools and repositories and all the merge request stuff. But, these things were not related to what guys wrote on comments and the stuff it was not connected.”

          Struggling with multiple tools, difficult build cycles, and an out of sync planning process in Jira, Hemmersbach needed a new process. They tried a wide variety of tools to solve the dilemma but still couldn’t achieve the speed they desired. During this time, teams were using GitLab Core and the business was impressed by how connected issues are with code repositories and how GitLab helped them improve team efficiency.  
      - title: Rebuilding the wheel to achieve speed and collaboration  
        description: |
          The Hemmersbach development group evaluated their process and overcame the challenges they were facing by rethinking how they worked together. They had a team utilizing GitLab and were impressed with how everything is connected to the commit and offers an overview for all of the pipeline steps — in one tool.

          GitLab Core provided built-in CI/CD, and project issue boards and teams were able to increase build speed and collaboration. Building on this momentum, they decided to rebuild the entire structure and move from scrum development to feature-driven development. While they saw progress, they still needed a tool that offered the ability to categorize items and then break them down so the team could continue to iterate faster.

          Gitlab Ultimate connected the portfolio planning process with the actual work to deliver new capabilities. The multi-level epics and roadmap review capabilities powered their teams to move at unprecedented speed. The code reviews and comments in GitLab helped improve overall code quality.
      - title: Rebuilding the wheel to achieve speed and collaboration
        description: |
          Using GitLab helped Hemmersbach decreased the time from planning to production by 6.5 days. By working in a single environment, they are also achieving 60 builds per day when previously they were only performing a single daily build.

          Hemmersbach has over 100 developers using GitLab on a daily basis to run their software. The deployments now begin with epics which allow everyone to collaborate as they work through the resulting issues and merge requests. Having all of the collaboration capabilities under one umbrella has enabled the company to bring teams together and achieve unprecedented deployment speed.

          “Everybody can see what’s going on across other projects as well,” Schmid said. “And then if there are comments or concerns with the code, you just write it, you just do it. GitLab has helped us bring teams together.”
    customer_success_stories:
      title: Customer success stories
      link:
        text: See all stories
        href: /customers/
      read_story: Read story
      stories:
        - title: Goldman Sachs
          image: /nuxt-images/customers/goldman_sachs_case_study.png
          description: Goldman Sachs improves from 1 build every two weeks to over a thousand per day
          link: /customers/goldman-sachs/
        - title: Siemens
          image: /nuxt-images/customers/siemens.png
          description: How Siemens created an open source DevOps culture with GitLab
          link: /customers/siemens
        - title: Fanatics
          image: /nuxt-images/customers/fanatics_case_study.png
          description: Fanatics' successful GitLab CI transition empowers innovation cycles and speed
          link: /customers/fanatics/
