data:
  customer: EAB
  customer_logo: /nuxt-images/logos/eab-logo.svg
  heading: 'Increasing development speed to keep students in school'
  header_image: /nuxt-images/blogimages/eab_case_study_Image.jpg
  customer_industry: Education
  customer_location: Washington D.C., Richmond VA, Minneapolis MN, Birmingham AL
  customer_employee_count: '1,300'
  customer_solution: |
    [GitLab Premium](/pricing/premium/){data-ga-name="premium solution" data-ga-location="customers hero"}
  key_benefits:
    - label: Reduced toolchain complexity
      icon: cog-code
    - label: Less overhead
      icon: speed-alt-2
    - label: Improved collaboration
      icon: collaboration-alt-4
  sticky_benefits:
  - stat: 18x
    label: faster builds
  - stat: 888
    label: projects across 80 groups
  - stat: 18,000
    label: builds
  blurb: 'EAB harnesses the collective power of 1,500+ schools, colleges, and universities
    to uncover proven solutions and transformative insights to keep students in school.'
  introduction: 'With GitLab, EAB can empower development teams with quality pipelines and collaboration by removing toolchain complexity and overhead.'
  quotes:
  - text: I really enjoy GitLab. I like the integration. It was easy to set up and
      it's not too complex or complicated. I could tell you that the most common stickers
      on our laptop in our offices are GitLab stickers, so all of our engineers like
      it.
    author: Brendan Mannix
    author_role: Vice President, Engineering
    author_company: EAB
  content: 
    - title: Using analytics to help students graduate
      subtitle: 
      description: >-
        EAB works with over 1,400 colleges, universities, community colleges, K-12 districts,
        independent schools, and graduate programs in the country to help keep students enrolled in school.
      

        The company’s flagship Student Success Management System, Navigate, helps identify at-risk students
        and allows staff to coordinate a plan for their success. It also helps identify students that wouldn’t
        traditionally be thought of as at-risk. These students often underperform in their major by avoiding
        specific classes or by putting their graduation date in jeopardy.



    - title: Overcoming toolchain complexity
      description: >-
          When EAB split from its parent company, they lost the licenses of their existing tools — giving them a chance to build a clean slate.
          Development teams consolidated tools to simplify their build process. Continuous Integration processes before the split required Bamboo,
          Jenkins, Solano, and Circle all running at different times and with each team running their own. This situation created unnecessary
          complexity and added time, as it would take six or eight hours to run the build.
      

          “Our build was just red. Like all the time red. Because the feedback loop was so long and the velocity at which people were trying to get stuff
          merged in was so high, that they basically ignored the build,” said Eliza Brock Marcum, CI Lead, EAB.



    - title: Auto-scaling to achieve faster pipelines
      description: >-
          EAB was looking for a solution for source control and CI that offered very lightweight maintenance overhead. They evaluated GitLab and decided that the monthly
          release cycle was extremely beneficial and provided the company with the future they were looking for.
      

          “GitLab from my perspective was vastly outpacing the competition in terms of feature releases. GitLab was later to the SCM game in comparison to GitHub or
          BitBucket, for instance, but with the rate of features being released it was very clear that you guys were gaining so much ground compared to the competition,” said Brendan Mannix,
          Vice President, Engineering, EAB. “It seems like over the long run that GitLab is going to be the right choice for EAB.”
      

          EAB is using on-premises GitLab CI and auto-scaling, which allows the company to have pipeline builds as fast as they want for an affordable price. The perception of the developers,
          of the quality of the product, has been changing consistently since the introduction of GitLab, and the CI runs passing green on a consistent basis.
      

          Developers are showing energy and enthusiasm when builds pass and are adding useful tests to individual contributions. A year ago, tests were not typically passing. Now that the builds
          are serving a purpose, people are reviewing them and collaborating more. The general skill level and enthusiasm for testing has improved.

    - title: Building a collaborative environment
      description: >-
          In large part because of the move to GitLab, EAB was able to get into an appropriate flow. The company now merges approved
          release branches into master branches by and everything is tested to make sure it is green. There are assigned approvers for merge
          requests and the company has controls in place on code merges.


          Within the first six months of using GitLab, EAB had 888 projects across 80 groups including subgroups as well with 214 users. The company had 18,000 builds,
          which comes down to almost half a million individual jobs.

