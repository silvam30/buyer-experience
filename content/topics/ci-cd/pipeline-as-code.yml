---
  title: What is pipeline as code?
  description: Pipeline as code uses source code, like Git, to create continuous integration pipelines. Learn the benefits of pipeline as code and how teams use it to configure builds and automate tests.
  date_published: 2023-04-06
  date_modified: 2023-04-06
  topics_header:
    data:
      title: What is pipeline as code?
      block:
        - metadata:
            id_tag: pipeline-as-code
          text: |
            Pipeline as code is a practice of defining deployment pipelines through source code, such as Git. Pipeline as code is part of a larger “as code” movement that includes infrastructure as code.
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: CI CD
      href: /topics/ci-cd/
      data-ga-name: ci-cd
      data_ga_location: breadcrumb
    - title: What is pipeline as code?
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: 'What are the benefits of pipeline as code'
          href: "#what-are-the-benefits-of-pipeline-as-code"
          data_ga_name: what are the benefits of pipeline as code
          data_ga_location: side-navigation
          variant: primary
        - text: 'Get started with CI/CD '
          href: "#get-started-with-cicd"
          data_ga_name: get started with CI/CD
          data_ga_location: side-navigation
          variant: primary
    content:
      - name: partners-copy-block
        data:
          no_decoration: true,
          column_size: 10
          blocks:
            - text: |
                \
                \
                With pipeline as code, teams can configure builds, tests, and deployment in code that is trackable and stored in a centralized source repository. Teams can use a declarative [YAML](https://about.gitlab.com/blog/2020/10/01/three-yaml-tips-better-pipelines/) approach or a vendor-specific programming language, such as Jenkins and Groovy, but the premise remains the same.

                A pipeline as code file specifies the stages, jobs, and actions for a pipeline to perform. Because the file is versioned, changes in pipeline code can be tested in branches with the corresponding application release.


                The pipeline as code model of creating [continuous integration pipelines](https://about.gitlab.com/blog/2019/07/12/guide-to-ci-cd-pipelines/) is an industry best practice, but deployment pipelines used to be created very differently.


                Early in [continuous integration](/topics/ci-cd/), deployment pipelines were set up as point-and-click or through a graphical user interface (GUI). This presented several challenges:


                * Auditing was limited to what was already built-in

                * Developers were unable to collaborate

                * Troubleshooting problems was difficult

                * Difficult to rollback changes to the last known configuration

                * Pipelines prone to breaking

                ### What are the benefits of pipeline as code? {#what-are-the-benefits-of-pipeline-as-code}


                The pipeline as code model corrected many of these pain points and offered the flexibility teams needed to execute efficiently.


                Pipeline as code comes with many of the [same benefits](https://martinfowler.com/bliki/InfrastructureAsCode.html) the other "as code" models have, such as:

                * **[Version control](https://about.gitlab.com/topics/version-control/):** Changes are trackable and teams can rollback to previous configurations.

                * **Audit trails:** Developers can see when changes were made to the source code and why.

                * **Ease of collaboration:** Code is available and visible for improvements, suggestions, and updates.

                * **Knowledge sharing:** Developers can share best practices, import templates, and link code snippets so teams can learn from each other.


                Pipeline as code also has operational and practical benefits:


                1. CI pipelines and application code are stored in the same source repository. All the information teams need is located in the same place.

                2. Developers can make changes without additional permissions and can work in the tools they’re already using.

                3. Teams can collaborate more efficiently. Keeping information accessible means teams can collaborate and then act on their decisions.

                4. Pipeline changes go through a code review process, avoiding any break in the pipeline integration.

                5. Deployment pipelines are in a version control system independent of continuous integration tools. Pipelines can be restored if the continuous integration system goes down. If a team wants to switch CI tools later on, pipelines can be moved into a new system.

                The pipeline as code model creates automated processes that help developers build applications more efficiently. Having everything documented in a [source repository](https://docs.gitlab.com/ee/user/project/repository/) allows for greater visibility and collaboration so that everyone can continually improve processes.

                ### Get started with CI/CD {#get-started-with-cicd}

                <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/sIegJaLy2ug" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

      - name: topics-cta
        data:
          title: What is Auto DevOps?
          text: |
            GitLab Auto DevOps eliminates the complexities of software delivery by automatically setting up the pipeline and integrations, which allows teams to focus on business productivity.
          column_size: 10
          cta_one:
            text: Learn More
            link: /stages-devops-lifecycle/auto-devops/
            data_ga_name: Learn More
            data_ga_location: body
  components:
    - name: solutions-resource-cards
      data:
        title: More on CI pipelines
        column_size: 4
        cards:
          - icon:
              name: webcast
              variant: marketing
              alt: webcast Icon
            event_type: "Webcast"
            header: "Mastering continuous software development"
            link_text: "Learn more"
            href: https://about.gitlab.com/webcast/mastering-ci-cd/
            image: /nuxt-images/blogimages/drupalassoc_cover.jpg
            data_ga_name: "Mastering continuous software development"
            data_ga_location: resource cards
          - icon:
              name: whitepapers
              variant: marketing
              alt: Whitepapers Icon
            event_type: "Whitepapers"
            header: "Scaled continuous integration and delivery"
            link_text: "Learn more"
            href: https://about.gitlab.com/resources/scaled-ci-cd/
            image: /nuxt-images/blogimages/covermga.jpg
            data_ga_name: "Scaled continuous integration and delivery"
            data_ga_location: resource cards
          - icon:
              name: whitepapers
              variant: marketing
              alt: Whitepapers Icon
            event_type: "Whitepapers"
            header: "The benefits of single application CI/CD"
            link_text: "Learn more"
            href: https://about.gitlab.com/resources/ebook-single-app-cicd/
            image: /nuxt-images/blogimages/cover_image_regenhu.jpg
            data_ga_name: "The benefits of single application CI/CD"
            data_ga_location: resource cards
          - icon:
              name: case-study
              variant: marketing
              alt: Case study Icon
            event_type: "Case Study"
            header: "How Paessler deploys up to 50 times daily with GitLab Premium"
            link_text: "Learn more"
            href: https://about.gitlab.com/customers/paessler-prtg/
            image: /nuxt-images/blogimages/paessler.jpg
            data_ga_name: "How Paessler deploys up to 50 times daily with GitLab Premium"
            data_ga_location: resource cards

    - name: solutions-resource-cards
      data:
        title: Suggested Content
        column_size: 4
        cards:
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: "How to streamline interactions between multiple repositories with multi-project pipelines"
            text: |
                  You can connect CI/CD pipelines and artifacts for multiple related projects to make managing interactions easy.
            link_text: "Learn more"
            href: https://about.gitlab.com/blog/2018/10/31/use-multiproject-pipelines-with-gitlab-cicd/
            image: /nuxt-images/blogimages/customers-link.jpg
            data_ga_name: "How to streamline interactions between multiple repositories with multi-project pipelines"
            data_ga_location: resource cards
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: "4 Benefits of CI/CD"
            text: |
                  Learn how to implement and measure a successful CI/CD pipeline strategy and help your DevOps team deliver higher quality software, faster!
            link_text: "Learn more"
            href: https://about.gitlab.com/blog/2019/06/27/positive-outcomes-ci-cd/
            image: /nuxt-images/blogimages/hotjar.jpg
            data_ga_name: "4 Benefits of CI/CD"
            data_ga_location: resource cards
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: "The business impact of CI/CD"
            text: |
                  How a good CI/CD strategy generates revenue and keeps developers happy.
            link_text: "Learn more"
            href: https://about.gitlab.com/blog/2019/06/21/business-impact-ci-cd/
            image: /nuxt-images/blogimages/modernize-cicd.jpg
            data_ga_name: "The business impact of CI/CD"
            data_ga_location: resource cards
