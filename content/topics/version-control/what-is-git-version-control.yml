---
  title: What is Git version control?
  description: Git is an open source distributed version control system that helps software teams create projects of all sizes with efficiency, speed, and asynchronicity.
  topics_header:
    data:
      title: What is Git version control?
      block:
        - metadata:
            id_tag: what-is-git-version-control
          text: Git is an open source distributed version control system that helps software teams create projects of all sizes with efficiency, speed, and asynchronicity.
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: Version Control
      href: /topics/version-control/
      data-ga-name: version-control
      data_ga_location: breadcrumb
    - title: What is Git version control?
  side_menu:
    anchors:
      text: "On this page"
    hyperlinks:
      text: ''
      data: []
    content:
      - name: topics-copy-block
        data:
          header: What is Git?
          column_size: 11
          blocks:
            - text: |
                Git is a [distributed version control system](/topics/version-control/){data-ga-name="version control" data-ga-location="body"} that enables software development teams to have multiple local copies of the project's codebase independent of each other. These copies, or branches, can be created, merged, and deleted quickly, empowering teams to experiment, with little compute cost, before merging into the main branch (sometimes referred to as the [master branch](/blog/2021/03/10/new-git-default-branch-name/){data-ga-name="master branch" data-ga-location="body"}). Git is known for its speed, [workflow compatibility](/blog/2020/04/07/15-git-tips-improve-workflow/){data-ga-name="workflow compatibility" data-ga-location="body"}, and open source foundation.


                Most Git actions only add data to the database, and Git makes it easy to undo changes during the three main states.


                Git has three file states: `modified`, `staged`, and `committed`.


                1. A `modified` file has been changed but isn't committed to the database yet.

                2. A `staged` file is set to go into the next commit.

                3. When a file is `committed`, the data has been stored in the database.


                With Git, software teams can experiment without fearing that they'll create lasting damage to the source code, because teams can always revert to a previous version if there are any problems.

      - name: topics-copy-block
        data:
          header: What is version control and how can you document changes?
          column_size: 11
          blocks:
            - text: |
                Version control — also known as source control or revision control — is software used to track revisions, solve integration conflicts in code, and manage different artifacts involved in software projects (e.g. design, data, images). Version control also enables frictionless communication, change, and reproducibility between developers and other team members.


                With version control, you're able to track and merge branches, audit changes, and enable concurrent work to accelerate software delivery. Version control is the foundation and the stepping stone of DevOps, software architecture, and release patterns, without which an org cannot start. Using version control results in remarkable team productivity and software quality improvements.

      - name: topics-copy-block
        data:
          header: Why should you use Git?
          column_size: 11
          blocks:
            - text: |
                Software development teams prefer Git over other version control systems, like CVS, Mercurial, and Perforce, because Git has the adaptability, speed, and stability required to thrive in fast-paced markets. It's no wonder that [87.2% of developers use Git](https://insights.stackoverflow.com/survey/2018/#work-_-version-control) for version control. If organizations want to quickly meet customer demand and business goals, Git version control is the simplest way to empower developers.


                [Git's ability](/blog/2020/03/13/partial-clone-for-massive-repositories/){data-ga-name="gits ability" data-ga-location="body"} to store a complete history of a project locally is a benefit, because Git branches are lightweight and the protocol is fast, so contributors can be anywhere, even with a poor connection and still sync a local copy with any other team member.


      - name: topics-copy-block
        data:
          header: The benefits of Git and a distributed version control system
          column_size: 11
          blocks:
            - text: |
                  ### Git has workflow flexibility


                  With Git, teams can work together using various branching strategies that are not as possible in other version control systems. Git's branching capabilities offer users the ability to select a workflow depending on the size of a project or team or unique processes. Some of the most popular Git workflows include [centralized](/topics/version-control/what-is-centralized-version-control-system/){data-ga-name="centralized" data-ga-location="body"}, feature branching, trunk-based development, and GitFlow.


                  ### Git is fast


                  Users have a local repository on their machine with a full history, so there's no lag in communicating with a server, which is an experience that users of centralized systems, such as CVS, Subversion, and Perforce, often encounter.


                  Having a local version control means Git doesn't have to ping a server to view a project's history to identify changes made between versions. Git can immediately do a local difference calculation.


                  ### Git is reliable


                  Git inherently has multiple backups, because each user has a local repository. If there's a crash, a copy could replace the main server. Another benefit of local repositories is that users can continue to commit offline if they're traveling or dealing with network issues.


                  ### Git is collaborative


                  Git makes collaborative development easy with its branching model. People on your team can create a branch, experiment, and merge the code to the main branch if it works well. Git tracks changes during code reviews and [team members can collaborate](/topics/version-control/software-team-collaboration/){data-ga-name="collaborate" data-ga-location="body"} in merge requests (or pull requests).
  
      - name: 'topics-copy-block'
        data:
          header: Learn about the basics of Git
          column_size: 12
          blocks:
            - text: |

              video:
                video_url: https://www.youtube-nocookie.com/embed/ZABsorovppA
  components:

    - name: topics-cta
      data:
        subtitle: Learn how GitLab streamlines software development
        text: GitLab streamlines software development with comprehensive version control and collaboration.
        column_size: 10
        cta_one:
          text: Learn More
          link: /stages-devops-lifecycle/source-code-management/
          data_ga_name: Learn More
          data_ga_location: body
    - name: solutions-resource-cards
      data:
        title: Want to learn more about Git?
        column_size: 4
        cards:
          - icon:
              name: article
              variant: marketing
              alt: article Icon
            event_type: "Case Study"
            header: Read how Remote uses GitLab to meet 100% of deadlines
            image: "/nuxt-images/resources/resources_20.jpg"
            link_text: "Learn more"
            href: /customers/remote/
            data_ga_name: Read how Remote uses GitLab to meet 100% of deadlines
            data_ga_location: body
          - icon:
              name: article
              variant: marketing
              alt: article Icon
            event_type: "Whitepaper"
            header: Learn how to move to Git
            image: "/nuxt-images/resources/resources_11.jpeg"
            link_text: "Learn more"
            href: /resources/whitepaper-moving-to-git/
            data_ga_name: Learn how to move to Git
            data_ga_location: body
