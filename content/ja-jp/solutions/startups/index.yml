---
  title: GitLab for Startups
  description: スタートアップを加速する単一のアプリケーション。 私たちは、資格基準を満たすスタートアップにトップランクを無料で提供しています。詳細はこちら!
  image_alt: GitLab for Startups
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        note:
          -  GitLab for startups
        title: 成長を加速
        subtitle: DevSecOpsプラットフォームでセキュリティを強化しながら、ソフトウェアをより速くリリースする
        aos_animation: fade-down
        aos_duration: 500
        title_variant: 'display1'
        mobile_title_variant: 'heading1-bold'
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          url: https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com
          text: 無料トライアルを開始
          data_ga_name: Start your free trial
          data_ga_location: header
        secondary_btn:
          url: /solutions/startups/
          text: GitLab for Startupsプログラムに参加する
          data_ga_name: startup join
          data_ga_location: header
          icon:
            variant: product
            name: chevron-lg-right
        image:
          image_url: /nuxt-images/solutions/startups_hero.jpeg
          alt: "イメージ: オープンソースのgitlab"
          bordered: true
    - name: customer-logos
      data:
        text_variant: body2
        text: 信頼者
        companies:
        - image_url: "/nuxt-images/logos/chorus-color.svg"
          link_label: Chorusお客様事例へのリンク
          alt: "Chorus ロゴ"
          url: /customers/chorus/
        - image_url: "/nuxt-images/logos/zoopla-logo.png"
          link_label: Zooplaお客様事例へのリンク
          alt: "zooplaロゴ"
          url: /customers/zoopla/
        - image_url: "/nuxt-images/logos/zebra.svg"
          link_label: Zebraお客様事例へのリンク
          alt: "zebraロゴ"
          url: /customers/thezebra/
        - image_url: "/nuxt-images/logos/hackerone-logo.png"
          link_label: Hackeroneお客様事例へのリンク
          alt: "hackeroneロゴ"
          url: /customers/hackerone/
        - image_url: "/nuxt-images/logos/weave_logo.svg"
          link_label: Weave お客様事例へのリンク
          alt: "weaveロゴ"
          url: /customers/weave/
        - image_url: /nuxt-images/logos/inventx.png
          alt: "inventxロゴ"
          url: /customers/inventx/

    - name: 'side-navigation-variant'
      links:
        - title: 概要
          href: '#overview'
        - title: メリット
          href: '#benefits'
        - title: 顧客
          href: '#customers'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content:

            - name: 'solutions-video-feature'
              data:
                header: 高速、効率的、そして制御。
                description: スタートアップにとって、ソフトウェアをより速く、より効率的にデプロイすることは良いことではありません。これは、顧客ベースの拡大、収益目標の達成、市場での製品の差別化になるかどうかとは異なります。 GitLabは、成長を加速するために構築されたDevSecOpsプラットフォームです。
                video:
                  url: 'https://player.vimeo.com/video/784035583?h=d978ef89bf&color=7759C2&title=0&byline=0&portrait=0'

        - name: 'div'
          id: benefits
          slot_enabled: true
          slot_content:
            - name: 'by-solution-value-prop'
              data:
                title: スタートアップがGitLabを選ぶ理由
                light_background: false
                large_card_on_bottom: true
                cards:
                  - title: ライフサイクル全体で1つのプラットフォーム
                    description: ソフトウェアデリバリーのライフサイクル全体にわたって1つのアプリケーションを使用することで、チームは多数のツールチェーン統合を維持するのではなく、優れたソフトウェアを提供することに集中できます。 コミュニケーション、エンドツーエンドの可視性、セキュリティ、サイクルタイム、オンボーディングなどを改善します。
                    href: /platform/?stage=plan
                    data_ga_name: platform plan
                    cta: 詳しく見る
                    icon:
                      name: cycle
                      alt: cycle Icon
                      variant: marketing
                  - title: さまざまな環境またはクラウドにデプロイする
                    description: GitLabはクラウドに依存しないため、必要な方法と場所で自由に使用でき、成長に合わせてクラウドプロバイダーを柔軟に変更および追加できます。AWS、Google Cloud、Azureなどにシームレスにデプロイします。
                    icon:
                      name: merge
                      alt: merge Icon
                      variant: marketing
                  - title: ビルトイン セキュリティ
                    description: GitLabを使用すると、ソフトウェアデリバリーライフサイクルのすべての段階にセキュリティとコンプライアンスが組み込まれ、チームが脆弱性をより速く特定し、開発をより迅速かつ効率的にできます。 スタートアップが成長するにつれて、スピードを失うことなくリスクを管理することができます。
                    href: /solutions/security-compliance/
                    data_ga_name: solutions dev-sec-ops
                    cta: 詳しく見る
                    icon:
                      name: devsecops
                      alt: devsecopsアイコン
                      variant: marketing
                  - title: ソフトウェアをより速く作成およびデプロイ
                    description: GitLabを使用すると、サイクルタイムを短縮し、より少ない労力でより頻繁にデプロイできます。新機能の計画から自動化されたテストおよびリリースオーケストレーションまで、すべてを単一のアプリケーションで行うことができます。
                    href: /platform/
                    data_ga_name: platform
                    cta: 詳しく見る
                    icon:
                      name: speed-gauge
                      alt: speed-gaugeアイコン
                      variant: marketing
                  - title: 強力なコラボレーション
                    description: サイロを解消し、開発、セキュリティ、運用チームからビジネスチームや非技術的な利害関係者まで、社内のすべての人がコードを使ってコラボレーションできるようにします。DevSecOpsプラットフォームを使用すると、ライフサイクル全体にわたって可視性を高め、ソフトウェアプロジェクトを進めるために関わるすべての人のコミュニケーションを向上することができます。
                    icon:
                      name: collaboration-alt-4
                      alt: コラボレーションアイコン
                      variant: marketing
                  - title: オープンソースプラットフォーム
                    description: GitLabのオープンコアは、オープンソースソフトウェアのすべての利点を提供します。これには、世界中の何千人もの開発者が継続的に改善と改良に取り組んでいるイノベーションを活用する機会も含まれます。
                    icon:
                      name: open-source
                      alt: オープンソースアイコン
                      variant: marketing
                  - title: GitLabはともにスケールします
                    description: シード資金からプラスのキャッシュフロー、大規模な多国籍企業（またはどこに行っても）まで、GitLabはあなたと一緒に成長します。
                    href: /customers/
                    data_ga_name: customers
                    cta: 詳しく見る
                    icon:
                      name: auto-scale
                      alt: オートスケールアイコン
                      variant: marketing
        - name: 'div'
          id: customers
          slot_enabled: true
          slot_content:
            - name: 'education-case-study-carousel'
              data:
                header: スタートアップをどのように支援してきたのか
                customers_cta: true
                cta:
                  href: '/customers/'
                  text: ストーリーをもっと表示
                  data_ga_name: customers
                case_studies:
                  - logo_url: "/nuxt-images/logos/chorus-color.svg"
                    institution_name: Chorus
                    quote:
                      img_url: /nuxt-images/blogimages/Chorus_case_study.png
                      quote_text: GitLabを使用すれば、プロダクトエンジニアリングとそれとやりとりしたい人は簡単につながることができます
                      author: Russell Levy
                      author_title: 共同創設者兼CTO、Chorus.ai
                    case_study_url: /customers/chorus/
                    data_ga_name: chorus case study
                    data_ga_location: case study carousel
                  - logo_url:  "/nuxt-images/logos/hackerone-logo.png"
                    institution_name: hackerone
                    quote:
                      img_url: /nuxt-images/blogimages/hackerone-cover-photo.jpg
                      quote_text: GitLabはセキュリティ上の欠陥を早期に発見し、開発者のフローに統合しています。エンジニアはコードをGitLab CIにプッシュし、多くの段階的な監査ステップの1つから即座にフィードバックを得て、そこにセキュリティの脆弱性が組み込まれているかどうかを確認し、さらには特定のセキュリティの問題に特化したテストを行う独自の新しいステップを構築することもできます。
                      author: Mitch Trale
                      author_title: HackerOneインフラストラクチャ主任
                    case_study_url: /customers/hackerone/
                    data_ga_name: hackerone case study
                    data_ga_location: case study carousel
                  - logo_url: /nuxt-images/logos/anchormen-logo.svg
                    institution_name: Anchormen
                    quote:
                      img_url: /nuxt-images/blogimages/anchormen.jpg
                      quote_text: 本当に仕事に集中できます。 GitLabをセットアップしたら、基本的にこのセーフティネットがあるので、他のすべてのことを考える必要はありません。 GitLabは保護し、ビジネスロジックに本当に焦点を当てることができます。 運用効率を向上させることは間違いないと思います。
                      author: Jeroen Vlek
                      author_title: AnchormenのCTO
                    case_study_url: /customers/anchormen/
                    data_ga_name: anchormen case study
                    data_ga_location: case study carousel


    - name: cta-block
      data:
        cards:
          - header: GitLab for Startupsプログラムがあなたに合っているかどうかを確認してください。
            icon: increase
            link:
              text: GitLab for Startupsプログラム
              url: /solutions/startups/join/
              data_ga_name: startup join
          - header: ツールチェーンの費用を確認しましょう。
            icon: piggy-bank-alt
            link:
              text: ROI計算
              url: /calculator/roi/
              data_ga_name: roi calculator
