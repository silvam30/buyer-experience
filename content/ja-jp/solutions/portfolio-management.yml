---
  title: ポートフォリオ管理
  description: GitLabのポートフォリオ管理により、組織全体を横断する大規模なプロジェクトを管理できます。
  components:
    - name: 'solutions-hero'
      data:
        title: ポートフォリオ管理
        subtitle: 組織全体を横断する大規模なプロジェクトを管理する
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          text: 無料トライアルを開始
          url: /free-trial/
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "画像:ポートフォリオ管理のGitLab"
    - name: 'solutions-feature-list'
      data:
        title: ポートフォリオのパフォーマンスを把握しながら、複数のアクティブなプログラムを追跡することは困難です。
        subtitle: GitLabポートフォリオ管理は、組織がビジネスのイニシアチブに応じて複数のプログラムの速度を計画、追跡、測定するのに役立ちます。
        icon:
          name: /nuxt-images/logos/gitlab.svg
          alt: GitLabアイコン
        features:
          - title: 大規模なプログラムを管理
            description: |
              大規模なプログラムには、プログラムがスケジュール通りに進むように計画、追跡、測定する必要があるさまざまなワークストリームがあります。

              * 複数の子エピックと関連するイシューを含む[マルチレベル](https://docs.gitlab.com/ee/user/group/epics/){data-ga-name="multi level epics" data-ga-location="body"}エピックを使用すると、組織は表示レベルと制御性を維持しながら、最大限の価値を提供するイニシアチブを優先できます。
              * エピックツリーをドラッグアンドドロップして、エピックとイシューを整理し、作業の優先順位を設定します。
            icon:
              use_icon_component: true
              name: ci-cd
              alt: CI-CDアイコン
              variant: marketing
              hex_color: '#9B51DF'
            image_url: /nuxt-images/solutions/portfolio-management/multi-level-epic.png
            image_alt: GitLabのマルチレベルエピック
            image_tagline: |
              [マルチレベルエピック](https://docs.gitlab.com/ee/user/group/epics/){data-ga-name="epics" data-ga-location="body"}
          - title: 大規模なプログラムのパフォーマンスを視覚化して測定
            description: |
              [ポートフォリオレベルのロードマップ](https://docs.gitlab.com/ee/user/group/roadmap/){data-ga-name="roadmap" data-ga-location="body"}ビューを使えば、製品のビジョン、戦略、ロードマップを確立し、クロスファンクショナルプログラムの進捗を把握できます。

              * 健康状態を表示して、[個々のイシューとエピックの健康状態](https://docs.gitlab.com/ee/user/project/issues/index.html#health-status){data-ga-name="health status" data-ga-location="body"}を特定、報告し、迅速に対応します。
              * [エピックボード](https://docs.gitlab.com/ee/user/group/epics/epic_boards.html){data-ga-name="epic boards" data-ga-location="body"}を使えば、かんばんスタイルのボードでエピックとワークフローを視覚化して追跡できます
            icon:
              use_icon_component: true
              name: source-code
              alt: ソースコードアイコン
              variant: marketing
              hex_color: '#52CDB7'
            image_url: /nuxt-images/solutions/portfolio-management/epic_view_roadmap.png
            image_alt: GitLabのポートフォリオレベルのロードマップ
            image_tagline: |
              [ポートフォリオレベルのロードマップ](https://docs.gitlab.com/ee/user/group/roadmap/){data-ga-name="portfolio level roadmap" data-ga-location="body"}
