---
  title: DevOpsライフサイクルにセキュリティを統合する
  description: 脆弱性管理とコンプライアンスとともに、DevSecOps CIパイプライン内で実施できる、SASTやDAST、Dependency Scanning、コンテナスキャンなどのGitLabのアプリケーションセキュリティテスト。
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        note:
          -  セキュリティとコンプライアンスのシフトレフト
        title: GitLabによるセキュリティとガバナンス
        subtitle: GitLabは、ソフトウェアデリバリを自動化し、エンドツーエンドのソフトウェアサプライチェーンを保護することで、スピードとセキュリティの両立を可能にします。
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Ultimateを無料で試す
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: ご質問があれば お問い合わせください
          url: /sales/
          data_ga_name: sales
          data_ga_location: hero
        image:
          image_url: /nuxt-images/solutions/gitlab-security-and-governance/gitlab-security-and-governance.jpg
          image_url_mobile: /nuxt-images/gitlab-security-and-governance/gitlab-security-and-governance.jpg
          alt: "画像: 民間セクターのGitLab"
          bordered: true
    - name: 'by-industry-intro'
      data:
        intro_text: 'お客様:'
        logos:
          - name: UBSのロゴ
            image: "/nuxt-images/home/logo_ubs_mono.svg"
            url: https://about.gitlab.com/blog/2021/08/04/ubs-gitlab-devops-platform/
            aria_label: UBSのお客様事例へのリンク
          - name: Hackeroneのロゴ
            image: /nuxt-images/logos/hackerone-logo.png
            url: https://about.gitlab.com/customers/hackerone/
            aria_label: HackerOneのお客様事例へのリンク
          - name: The Zebraのロゴ
            image: /nuxt-images/logos/zebra.svg
            url: https://about.gitlab.com/customers/thezebra/
            aria_label: The Zebraのお客様事例へのリンク
          - name: ヒルティのロゴ
            image: /nuxt-images/logos/hilti_logo.svg
            url: https://about.gitlab.com/customers/hilti/
            aria_label: ヒルティのお客様事例へのリンク
          - name: Conversicaのロゴ
            image: /nuxt-images/logos/conversica.svg
            url: https://about.gitlab.com/customers/conversica/
            aria_label: Conversicaのお客様事例へのリンク
          - name: Bendigo and Adelaide Bankのロゴ
            image: /nuxt-images/logos/bendigo_and_adelaide_bank.svg
            url: https://about.gitlab.com/customers/bab/
            aria_label: Bendigo and Adelaide Bankのお客様事例へのリンク
          - name: Glympseのロゴ
            image: /nuxt-images/logos/glympse-logo-mono.svg
            url: https://about.gitlab.com/customers/glympse/
            aria_label: Glympseのお客様事例へのリンク
    - name: 'side-navigation-variant'
      links:
        - title: 概要
          href: '#overview'
        - title: メリット
          href: '#benefits'
        - title: 機能
          href: '#capabilities'
        - title: 顧客
          href: '#customers'
        - title: 価格
          href: '#pricing'
        - title: リソース
          href: '#resources'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-benefits'
              data:
                title: スピードとセキュリティを提供
                is_accordion: false
                items:
                  - icon:
                      name: continuous-integration
                      alt: 継続的インテグレーションアイコン
                      variant: marketing
                    header: 統合セキュリティ
                    text: 1つのプラットフォームで追加コストが発生することなく、すべてをすぐに使えます。
                    link_text: DevSecOpsプラットフォームの詳細について
                    link_url: /platform
                    ga_name: platform
                    ga_location: overview
                  - icon:
                      name: devsecops
                      alt: DevSecOpsアイコン
                      variant: marketing
                    header: 継続的なセキュリティ
                    text: コードプッシュ前後の自動スキャン。
                    link_text: GitLabを選ぶ理由
                    link_url: /why-gitlab/
                    ga_name: why gitlab
                    ga_location: overview
                  - icon:
                      name: release
                      alt: チェックマーク付きの盾アイコン
                      variant: marketing
                    header: 完全な制御
                    text: ガードレールを実装し、ポリシーを自動化します。
                    link_text: プラットフォームのアプローチの詳細について
                    ga_name: devops platform
                    ga_location: overview
                    link_url: /solutions/devops-platform/
            - name: 'solutions-video-feature'
              data:
                video:
                  url: 'https://player.vimeo.com/video/762685637?h=f96e969756'
        - name: 'div'
          id: 'benefits'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-value-prop'
              data:
                title: セキュリティとコンプライアンスをシフトレフト
                cards:
                  - title: ソフトウェアサプライチェーンの保護
                    description: GitLabは、エンドツーエンドでソフトウェアサプライチェーン（ソース、ビルド、依存関係、リリースされたアーティファクトを含む）を保護し、使用されているソフトウェアのインベントリ（ソフトウェア部品表）を作成し、必要な制御を適用するのに役立ちます。
                    data_ga_name: supply chain
                    data_ga_location: benefits
                    cta: もっと詳しく知る
                    icon:
                      name: less-risk
                      alt: 低リスクアイコン
                      variant: marketing
                    href: /solutions/supply-chain/
                  - title: 脅威ベクトルの管理
                    description: GitLabは、ソースコード、コンテナ、依存関係、実行中のアプリケーションの脆弱性を自動的にスキャンすることで、セキュリティのシフトレフトをサポートします。本番環境を保護するためにガードレール制御を実装できます。
                    data_ga_name: docs app security
                    data_ga_location: benefits
                    cta: もっと詳しく知る
                    icon:
                      name: devsecops
                      alt: DevSecOpsアイコン
                      variant: marketing
                    href: https://docs.gitlab.com/ee/user/application_security/get-started-security.html
                  - title: コンプライアンス要求事項の遵守
                    description: GitLabは、変更を追跡し、本番環境に反映される内容を保護するために必要な制御を実装し、ライセンスのコンプライアンスと規制のフレームワークを確実に遵守できるよう支援します。
                    data_ga_name: compliance
                    data_ga_location: benefits
                    cta: もっと詳しく知る
                    icon:
                      name: eye-magnifying-glass
                      alt: 虫眼鏡アイコン
                      variant: marketing
                    href: /solutions/compliance/
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-solutions-block'
              data:
                subtitle: セキュリティをシフトレフト
                sub_description: ''
                white_bg: true
                sub_image: /nuxt-images/solutions/dev-sec-ops/dev-sec-ops.png
                solutions:
                  - title: CI/CDパイプライン内にセキュリティテストを統合
                    description: 内蔵スキャナーを使用し、カスタムスキャナーを統合しましょう。セキュリティをシフトレフトして、デベロッパーがセキュリティの欠陥をすぐに見つけて修正できるようにします。包括的なスキャナーには、SAST、DAST、秘密スキャン、Dependency Scanning、コンテナスキャン、IaCスキャン、APIセキュリティ、ファジングテストが含まれます。
                    link_text: 詳しく見る
                    link_url: https://docs.gitlab.com/ee/user/application_security/?_gl=1*hwzvlj*_ga*NTg0MjExODQyLjE2MTk1MzkzOTQ.*_ga_ENFH3X7M5Y*MTY2MzIyOTY1My44OS4xLjE2NjMyMzAyNTYuMC4wLjA.
                    data_ga_name: CI/CD
                    data_ga_location: solutions block
                  - title: 依存関係を管理
                    description: ソフトウェア開発で現在使用されている多数のオープンソースコンポーネントを考えると、これらの依存関係を手動で管理することは困難な作業です。アプリケーションとコンテナの依存関係をスキャンしてセキュリティ上の欠陥を確認し、使用されている依存関係のソフトウェア部品表(SBOM)を作成します。
                    link_text: 詳しく見る
                    link_url: https://docs.gitlab.com/ee/user/application_security/dependency_list/
                    data_ga_name: dependencies management
                    data_ga_location: solutions block
                  - title: 脆弱性を管理
                    description: デベロッパーの普段のワークフローに潜む脆弱性を明らかにし、本番環境にコードをプッシュする前に解決することで、セキュリティチームを拡張します。セキュリティ担当者は、パイプライン、オンデマンドスキャン、サードパーティ、バグ報奨金によって、脆弱性をすべて1か所で精査し、トリアージし、管理できます。
                    link_text: 詳しく見る
                    link_url: https://docs.gitlab.com/ee/user/application_security/vulnerability_report/
                    data_ga_name: vulnerability management
                    data_ga_location: solutions block
                  - title: 実行中のアプリケーションを保護
                    description: クラスターでセキュアなCI/CDトンネルを設定し、動的アプリケーションセキュリティスキャンや運用コンテナスキャンを実行し、IPホワイトリストを設定することで、ワークロードを保護します。
                    link_text: 詳しく見る
                    link_url: https://docs.gitlab.com/ee/user/application_security/
                    data_ga_name: secure applications
                    data_ga_location: solutions block
                  - title: ガードレールを実装し、コンプライアンスを確保
                    description: ソフトウェア開発ライフサイクル全体でセキュリティとコンプライアンスポリシーを自動化します。コンプライアンスパイプラインは、パイプラインポリシーが回避されないことを保証し、共通制御はエンドツーエンドのガードレールを提供します。
                    link_text: 詳しく見る
                    link_url: https://docs.gitlab.com/ee/administration/compliance.html
                    data_ga_name: guardrails guardrails
                    data_ga_location: solutions block
        - name: 'div'
          id: 'customers'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-quotes-carousel'
              data:
                align: left
                header: |
                  企業に信頼され、
                  <br />
                  デベロッパーに愛用されています。
                quotes:
                  - title_img:
                      url: /nuxt-images/logos/hackerone-logo.png
                      alt: Hackeroneロゴ
                    quote: セキュリティ上の欠陥を早期に発見する上でGitLabは役立っており、デベロッパーのフローに統合されています。エンジニアはコードをGitLab CIにプッシュし、多くの段階的な監査ステップの1つから即座にフィードバックを得て、そこにセキュリティの脆弱性が組み込まれているかどうかを確認し、さらには特定のセキュリティの問題に特化したテストを行う独自の新しいステップを構築することもできます。
                    author: Mitch Trale氏
                    position: Hackerone、インフラストラクチャ主任
                    ga_carousel: hackerone testimonial
                    ga_location: customers
                    url: /customers/hackerone/
                  - title_img:
                      url: /nuxt-images/logos/bendigo_and_adelaide_bank.svg
                      alt: Bendigo and Adelaide Bankロゴ
                    quote: 私たちには今、デジタルトランスフォーメーションの目標に沿った、常に革新し続けるソリューションがあります。
                    author: Caio Trevisan氏
                    position: Bendigo and Adelaide Bank、DevOps Enablement主任
                    ga_carousel: bendigo and adelaide bank testimonial
                    ga_location: customers
                    url: /customers/bab/
                  - title_img:
                      url: /nuxt-images/logos/zebra.svg
                      alt: The Zebraロゴ
                    quote: (GitLabの)最大の価値は、開発チームがデプロイプロセスでより大きな役割を果たせるようになることです。以前は、どのような仕組みであるかを本当にわかっている人はほんのわずかでしたが、今では開発組織全体がCIパイプラインの仕組みを知っていて、使用することができ、新しいサービスを追加し、インフラストラクチャがボトルネックになることなく本番環境に移行できます。
                    author: Dan Bereczki氏
                    position: The Zebra、シニアソフトウェアマネージャー
                    ga_carousel: the zebra testimonial
                    ga_location: customers
                    url: /customers/thezebra/
                  - title_img:
                      url: /nuxt-images/logos/hilti_logo.svg
                      alt: ヒルティロゴ
                    quote: GitLabはスイートのようにバンドルされており、非常に洗練されたインストーラーが付属しています。そしてどういうわけかうまく機能します。これは、ただ立ち上げて稼動させたい企業にとっては非常に便利です。
                    author: Daniel Widerin氏
                    position: ヒルティ、ソフトウェアデリバリ主任
                    ga_location: customers
                    ga_carousel: hilti testimonial
                    url: /customers/hilti/
        - name: div
          id: 'pricing'
          slot_enabled: true
          slot_content:
            - name: 'tier-block'
              class: 'slp-mt-96'
              id: 'pricing'
              data:
                header: あなたに最適なプランはどれですか?
                cta:
                  url: /pricing/
                  text: 価格の詳細について
                  data_ga_name: pricing learn more
                  data_ga_location: pricing
                  aria_label: 価格
                tiers:
                  - id: free
                    title: Free
                    items:
                      - 静的アプリケーションセキュリティテスト(SAST)と秘密検出
                      - jsonファイルの所見
                    link:
                      href: /pricing/
                      text: 詳しく見る
                      data_ga_name: free learn more
                      data_ga_location: pricing
                      aria_label: Freeプラン
                  - id: premium
                    title: Premium
                    items:
                      - 静的アプリケーションセキュリティテスト(SAST)と秘密検出
                      - jsonファイルの所見
                      - MRの承認とその他の一般的な制御
                    link:
                      href: /pricing/
                      text: 詳しく見る
                      data_ga_name: premium learn more
                      data_ga_location: pricing
                      aria_label: Premiumプラン
                  - id: ultimate
                    title: Ultimate
                    items:
                      - Premiumの機能に加え、以下の機能が含まれます
                      - 包括的なセキュリティスキャナー(SAST、DAST、秘密、依存関係、コンテナ、IaC、API、クラスターイメージ、ファジングテストなど)
                      - MRパイプライン内での実行可能な結果
                      - コンプライアンスパイプライン
                      - セキュリティとコンプライアンスのダッシュボード
                      - その他たくさん
                    link:
                      href: /pricing/
                      text: 詳しく見る
                      data_ga_name: ultimate learn more
                      data_ga_location: pricing
                      aria_label: Ultimateプラン
                    cta:
                      href: /free-trial/
                      text: Ultimateを無料で試す
                      data_ga_name: try ultimate
                      data_ga_location: pricing
                      aria_label: Ultimateプラン
        - name: 'div'
          id: 'resources'
          slot_enabled: true
          slot_content:
          - name: solutions-resource-cards
            data:
              title: 関連リソース
              align: left
              column_size: 4
              grouped: true
              cards:
                - icon:
                    name: video
                    variant: marketing
                    alt: ビデオアイコン
                  event_type: "ビデオ"
                  header: セキュリティのシフトレフト - GitLabセキュリティの概要
                  link_text: "今すぐ視聴"
                  image: "/nuxt-images/solutions/dev-sec-ops/shifting-security-left.jpeg"
                  href: https://www.youtube.com/embed/XnYstHObqlA?enablejsapi=1
                  data_ga_name: Shifting Security Left - GitLab Security Overview
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: ビデオアイコン
                  event_type: "ビデオ"
                  header: GitLabを用いた脆弱性の管理と職務分掌の実施
                  link_text: "今すぐ視聴"
                  image: /nuxt-images/solutions/dev-sec-ops/managing-vulnerabilities.jpeg
                  href: https://www.youtube.com/embed/J5Frv7FZtnI?enablejsapi=1
                  data_ga_name: Managing Vulnerabilities and Enabling Separation of Duties with GitLab
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: ビデオアイコン
                  event_type: "ビデオ"
                  header: GitLab 15のリリース - 新しいセキュリティ機能
                  link_text: "今すぐ視聴"
                  image: "/nuxt-images/solutions/dev-sec-ops/gitlab-15-release.jpeg"
                  href: https://www.youtube.com/embed/BasGVNvOFGo?enablejsapi=1
                  data_ga_name: GitLab 15 Release - New Security Features
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: ビデオアイコン
                  event_type: "ビデオ"
                  header: SBOMと認証
                  link_text: "今すぐ視聴"
                  image: "/nuxt-images/solutions/compliance/sbom-and-attestation.jpeg"
                  href: https://www.youtube.com/embed/wX6aTZfpJv0?enablejsapi=1
                  data_ga_name: SBOM and Attestation
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: 電子書籍のアイコン
                  event_type: 書籍
                  header: "ソフトウェアサプライチェーンのセキュリティガイド"
                  link_text: "詳しく見る"
                  image: "/nuxt-images/resources/resources_11.jpeg"
                  href: https://cdn.pathfactory.com/assets/10519/contents/360915/35d042c6-3449-4d50-b2e9-b08d9a68f7a1.pdf
                  data_ga_name: "Guide to software supply chain security"
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: 電子書籍のアイコン
                  event_type: "書籍"
                  header: "GitLab CI/CDを使用したDevSecOps"
                  link_text: "詳しく見る"
                  image: "/nuxt-images/resources/resources_1.jpeg"
                  href: https://page.gitlab.com/achieve-devsecops-cicd-ebook.html
                  data_ga_name: "DevSecOps with GitLab CI/CD"
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: 電子書籍のアイコン
                  event_type: "書籍"
                  header: "GitLab DevSecOpsアンケート"
                  link_text: "詳しく見る"
                  image: "/nuxt-images/resources/fallback/img-fallback-cards-infinity.png"
                  href: https://cdn.pathfactory.com/assets/10519/contents/432983/c6140cad-446b-4a6c-96b6-8524fac60f7d.pdf
                  data_ga_name: "GitLab DevSecOps survey"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: ブログのアイコン
                  event_type: "ブログ"
                  header: "シフトレフトするための9つのヒント"
                  link_text: "詳しく見る"
                  href: https://about.gitlab.com/blog/2020/06/23/efficient-devsecops-nine-tips-shift-left/
                  image: /nuxt-images/blogimages/hotjar.jpg
                  data_ga_name: "9 tips to shift left"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: ブログのアイコン
                  event_type: "ブログ"
                  header: "自動認証によるソフトウェアサプライチェーンの保護"
                  link_text: "詳しく見る"
                  href: https://about.gitlab.com/blog/2022/08/10/securing-the-software-supply-chain-through-automated-attestation/
                  image: /nuxt-images/blogimages/cover_image_regenhu.jpg
                  data_ga_name: "Securing the software supply chain through automated attestation"
                  data_ga_location: resource cards
                - icon:
                    name: report
                    alt: レポートアイコン
                    variant: marketing
                  event_type: "アナリストレポート"
                  header: "451 Researchの見解: DevOpsの視野を広げるGitLab"
                  link_text: "詳しく見る"
                  href: "https://page.gitlab.com/resources-report-451-gitlab-broadens-devops.html"
                  image: /nuxt-images/blogimages/hemmersbach_case_study.jpg
                  data_ga_name: "451 Research opinion: GitLab broadens view of DevOps"
                  data_ga_location: resource cards
                - icon:
                    name: report
                    alt: レポートアイコン
                    variant: marketing
                  event_type: "アナリストレポート"
                  header: "GitLabが2022 Gartner Magic Quadrantで挑戦者に認定"
                  link_text: "詳しく見る"
                  href: "https://about.gitlab.com/analysts/gartner-ast22/"
                  image: /nuxt-images/blogimages/fjct_cover.jpg
                  data_ga_name: "GitLab a challenger in 2022 Gartner Magic Quadrant"
                  data_ga_location: resource cards
    - name: 'report-cta'
      data:
        layout: "dark-shortened"
        title: アナリストレポート
        reports:
        - description: "GitLabがForrester Wave™: Integrated Software Delivery Platforms, Q 2 2023の唯一のリーダーに認定"
          url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
          link_text: 報告書を読む
        - description: "DevOpsプラットフォームの2023 Gartner® Magic Quadrant™でGitLabがリーダーに認定"
          url: /gartner-magic-quadrant/
          link_text: 報告書を読む
    - name: solutions-cards
      data:
        title: GitLabでさらに多くのことを実現
        column_size: 4
        link :
          url: /solutions/
          text: その他のソリューションを確認
          data_ga_name: all solutions
          data_ga_location: do more with gitlab
        cards:
          - title: 継続的なソフトウェアコンプライアンス
            description: GitLabなら、DevOpsライフサイクルにセキュリティを統合するのも簡単です。
            icon:
              name: continuous-integration
              alt: 継続的インテグレーションアイコン
              variant: marketing
            href: /solutions/compliance/
            cta: もっと詳しく知る
            data_ga_name: compliance
            data_ga_location: do more with gitlab
          - title: ソフトウェアサプライチェーンセキュリティ
            description: ソフトウェアサプライチェーンの安全性とコンプライアンスを保証します。
            icon:
              name: devsecops
              alt: DevSecOpsアイコン
              variant: marketing
            href: /solutions/supply-chain/
            cta: もっと詳しく知る
            data_ga_name: supply chain
            data_ga_location: do more with gitlab
          - title: 継続的インテグレーションとデリバリ
            description: ソフトウェアデリバリをオンデマンドで繰り返し実現します。
            icon:
              name: continuous-delivery
              alt: 継続的デリバリ
              variant: marketing
            href: /solutions/continuous-integration/
            cta: もっと詳しく知る
            data_ga_name: ci
            data_ga_location: do more with gitlab
